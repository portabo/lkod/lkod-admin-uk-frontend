"use client";
import React, { useState } from "react";

import Button from "@/components/Button";
import { FolderPlusIcon } from "@/components/icons/FolderPlusIcon";
import styles from "@/organizations/Organizations.module.scss";
import AddUserDialog from "@/users/(components)/AddUserDialog";

const AddUserButton = () => {
    const [addNewOpen, setAddNewOpen] = useState(false);

    return (
        <>
            <Button
                color={`secondary`}
                label={`Přidat uživatele`}
                className={styles["add-user"]}
                onClick={() => {
                    setAddNewOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                }}
            >
                <FolderPlusIcon color="tertiary" />
            </Button>
            <AddUserDialog isOpen={addNewOpen} closeDialog={() => setAddNewOpen(false)} />
        </>
    );
};

export default AddUserButton;
