import { ApiClient, ResponseType } from "./api-client";

export class StatusService {
    /**
     * Availability of storage
     */
    async getFilesStatus(): Promise<ResponseType<void>> {
        return ApiClient.get("status/files");
    }
}
