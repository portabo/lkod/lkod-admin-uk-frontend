"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

import text from "@/textContent/cs.json";

import styles from "./Navigation.module.scss";

const Navigation = () => {
    const pathname = usePathname();
    return (
        <nav className={styles.navigation}>
            <ul>
                <li>
                    <Link href={"/instructions/guidance"} className={pathname === "/instructions/guidance" ? styles.active : ""}>
                        {text.instructionsPage.InstructionsForUse}
                    </Link>
                </li>
                <li>
                    <Link
                        href={"/instructions/registration"}
                        className={pathname === "/instructions/registration" ? styles.active : ""}
                    >
                        {text.instructionsPage.RegistrationInNKOD}
                    </Link>
                </li>
            </ul>
        </nav>
    );
};

export default Navigation;
