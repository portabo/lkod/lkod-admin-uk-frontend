import React from "react";

import { CardInfo } from "@/components/CardInfo";
import { Heading } from "@/components/Heading";
import { NoResultIcon } from "@/components/icons/NoResultIcon";
import { Paragraph } from "@/components/Paragraph";
import text from "@/textContent/cs.json";

import styles from "./NoResult.module.scss";

export const NoResult = () => {
    return (
        <CardInfo className={styles["no-result"]}>
            <NoResultIcon />
            <Heading tag={`h2`}>{text.noResultHeading}</Heading>
            <Paragraph size={`lg`} className={styles.text}>
                {text.noResultText}
            </Paragraph>
        </CardInfo>
    );
};
