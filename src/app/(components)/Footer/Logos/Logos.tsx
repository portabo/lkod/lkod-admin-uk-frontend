import React from "react";

import { Paragraph } from "@/components/Paragraph";
import logo_golemio from "@/images/logos/logo_golemi_gray.svg";
import logo_oict from "@/images/logos/logo_oict_gray.svg";
import logo_portalhmlp from "@/images/logos/logo_portalhlmp_gray.svg";
import text from "@/textContent/cs.json";

import SocialIcon from "../SocialIcon/SocialIcon";
import styles from "./Logos.module.scss";

const logos = [
    { name: "Logo OICT", url: logo_oict, link: "https://operatorict.cz/" },
    { name: "Logo Golemio", url: logo_golemio, link: "https://golemio.cz/" },
    { name: "Logo Portál Hl.m.P.", url: logo_portalhmlp, link: "https://www.praha.eu/jnp/" },
];

const Logos = () => {
    return (
        <div className={styles["logos-container"]}>
            <Paragraph>{text.footer.logosHeading}</Paragraph>
            <div className={styles["logos"]}>
                {logos.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} isLogo />;
                })}
            </div>
        </div>
    );
};

export default Logos;
