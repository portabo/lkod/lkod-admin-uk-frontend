"use client";
import Link from "next/link";
import { Session } from "next-auth";
import React, { useState } from "react";

import { BurgerIcon } from "@/components/icons/BurgerIcon";
import { CloseIcon } from "@/components/icons/CloseIcon";
import { UserRole } from "@/services/authorization-service";
import text from "@/textContent/cs.json";

import { LoginIcon } from "../icons/LoginIcon";
import { LogOutIcon } from "../icons/LogOutIcon";
import styles from "./NavBar.module.scss";

interface NavBarProps {
    session: Session | null;
}
export const NavBar = ({ session }: NavBarProps) => {
    const [open, setOpen] = useState(false);

    const toggleHandler = () => {
        setOpen(!open);
    };

    return (
        <nav className={styles.navbar}>
            <button className={styles["navbar__button"]} onClick={toggleHandler} aria-label={`Menu`} type="button">
                {open ? <CloseIcon color={`tertiary`} /> : <BurgerIcon color={"tertiary"} />}

                <span className={styles["navbar__title"]}>MENU</span>
            </button>

            <div className={`${styles["navbar__menu"]} ${open ? styles["open"] : ""}`}>
                {session?.user?.email ? (
                    <ul>
                        <li>
                            <Link href={`/`} onClick={toggleHandler}>
                                {text.datasetsTitle}
                            </Link>
                        </li>
                        {session?.user?.role == UserRole.superadmin && (
                            <>
                                <li>
                                    <Link onClick={toggleHandler} href={`/organizations`}>
                                        {text.organization.organizations}
                                    </Link>
                                </li>
                                <li>
                                    <Link onClick={toggleHandler} href={`/users`}>
                                        {text.users.users}
                                    </Link>
                                </li>
                            </>
                        )}
                        <li>
                            <Link onClick={toggleHandler} href={`/change-password`}>
                                {text.password.changePassword}
                            </Link>
                        </li>
                        <li className={styles.user}>{session?.user?.email}</li>
                        <li>
                            <Link className={styles["red-link"]} onClick={toggleHandler} href={`/signout`}>
                                {text.login.logMeOut}
                                <LogOutIcon color={`secondary`} />
                            </Link>
                        </li>
                    </ul>
                ) : (
                    <ul>
                        <li>
                            <Link className={styles["red-link"]} onClick={toggleHandler} href={"/login"}>
                                {text.login.login}
                                <LoginIcon color={`secondary`} />
                            </Link>
                        </li>
                    </ul>
                )}
            </div>
        </nav>
    );
};
