import { DetailedHTMLProps } from "react";

import styles from "./Chip.module.scss";

interface Props extends DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    label: string;
    onClick?: () => void;
}

export const Chip = ({ label, onClick, ...props }: Props) => {
    return (
        <div className={styles.chip} {...props}>
            {label}
            {onClick && (
                <button className={styles.close} onClick={onClick}>
                    &times;
                </button>
            )}
        </div>
    );
};
