"use server";

import { ResponseType } from "@/services/api-client";
import { ApiClient } from "@/services/initApi";
import { Organization, OrganizationFields } from "@/services/organizations-service";

export async function deleteOrganization(id: number): Promise<ResponseType<void>> {
    const { organizationsApi } = ApiClient;
    return await organizationsApi.deleteOrganization(id);
}

export async function updateOrganization(id: number, org: OrganizationFields): Promise<ResponseType<Organization>> {
    const { organizationsApi } = ApiClient;
    return await organizationsApi.updateOrganization(id, org);
}

export async function addOrganization(org: OrganizationFields): Promise<ResponseType<Organization>> {
    const { organizationsApi } = ApiClient;
    return await organizationsApi.addOrganization(org);
}

export async function addOrganizationMember(organizationId: number, userId: number): Promise<ResponseType<void>> {
    const { organizationsApi } = ApiClient;
    return await organizationsApi.addOrganizationMember(organizationId, userId);
}

export async function removeOrganizationMember(organizationId: number, userId: number): Promise<ResponseType<void>> {
    const { organizationsApi } = ApiClient;
    return await organizationsApi.removeOrganizationMember(organizationId, userId);
}
