export interface IListFormat {
    iri: string;
    label: string;
    count: string;
}

export interface IListKeyword {
    label: string;
    count: string;
}

export interface IListTheme {
    iri: string;
    label: string;
    count: string;
}

export interface IListPublisher {
    iri: string;
    label: string;
    count: string;
}

export interface IRawPublisherData {
    iri: string;
    name: string;
    logo?: string;
    description?: string;
    count: string;
}

export interface IPublisherData {
    iri: string;
    name: string;
    logo: string | null;
    description: string | null;
    count: number;
}

export interface IRawThemeData {
    iri: string;
    title: string;
    image?: string;
    count: string;
}

export interface IThemeData {
    iri: string;
    title: string;
    image: string | null;
    count: number;
}
