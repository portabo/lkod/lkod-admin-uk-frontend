import React from "react";
import OrganizationForm from "src/app/organizations/(components)/OrganizationForm";

import { Modal } from "@/components/Modal";

interface AddOrganizationDialogProps {
    isOpen: boolean;
    closeDialog: () => void;
}

const AddOrganizationDialog = (props: AddOrganizationDialogProps) => {
    return (
        <Modal show={props.isOpen} onClose={props.closeDialog} label={"Přidat organizaci"}>
            <OrganizationForm />
        </Modal>
    );
};

export default AddOrganizationDialog;
