import { useEffect, useMemo, useState } from "react";

import { ApiClient } from "@/services/initApi";

export const useFilesStatus = () => {
    const [areFilesAvailable, setFilesAvailable] = useState(false);
    const { statusApi } = ApiClient;

    useEffect(() => {
        statusApi
            .getFilesStatus()
            .then(() => setFilesAvailable(true))
            .catch(() => setFilesAvailable(false));
    }, [statusApi]);

    const ret = useMemo(() => {
        return { areFilesAvailable };
    }, [areFilesAvailable]);

    return ret;
};
