"use client";
/* eslint-disable react-hooks/exhaustive-deps */
import isDeepEqual from "fast-deep-equal/es6/react";
import { use, useEffect, useRef, useState } from "react";

import { IListFormat, IListKeyword, IListPublisher, IListTheme } from "@/(schema)";
import {
    useFormatsPromise,
    useKeywordsPromise,
    usePublishersPromise,
    useStatusesPromise,
    useThemesPromise,
} from "@/context/DataProvider";

import { IStatus } from "../(types)/LookupTypes";
import { useUrlFilter } from "./useUrlFilter";

export const useFilterChips = () => {
    const publishersData = use(usePublishersPromise());
    const topicsData = use(useThemesPromise());
    const keywordsData = use(useKeywordsPromise());
    const formatsData = use(useFormatsPromise());
    const statusesData = use(useStatusesPromise());
    const { urlFilter } = useUrlFilter();

    const [publishers, setPublishers] = useState<IListPublisher[]>();
    const [topics, setTopics] = useState<IListTheme[]>();
    const [keys, setKeys] = useState<IListKeyword[]>();
    const [formats, setFormats] = useState<IListFormat[]>();
    const [statuses, setStatuses] = useState<IStatus[]>();

    const { publisherIri, themeIris, keywords, formatIris, status } = urlFilter;

    const keywordsRef = useRef(keywords);
    const themeIrisRef = useRef(themeIris);
    const formatIrisRef = useRef(formatIris);
    const statusRef = useRef(status);

    if (!isDeepEqual(keywordsRef.current, keywords)) {
        keywordsRef.current = keywords;
    }
    if (!isDeepEqual(themeIrisRef.current, themeIris)) {
        themeIrisRef.current = themeIris;
    }

    if (!isDeepEqual(formatIrisRef.current, formatIris)) {
        formatIrisRef.current = formatIris;
    }

    if (!isDeepEqual(urlFilter.status, status)) {
        statusRef.current = status;
    }

    const getPublisherChip = (queryPublisher?: string) => {
        let publishers: IListPublisher[];
        if (!queryPublisher) {
            publishers = [];
        } else {
            publishers = [publishersData[publishersData.findIndex((element: IListPublisher) => element.iri === queryPublisher)]];
        }

        setPublishers(publishers);
    };

    const getTopicChips = (queryTopic?: string | string[]) => {
        let topics: IListTheme[];
        if (!queryTopic) {
            topics = [];
        } else if (typeof queryTopic === "string") {
            topics = [topicsData[topicsData.findIndex((element) => element.iri === queryTopic)]];
        } else {
            topics = queryTopic.map((el) => topicsData.filter((e) => e.iri === el)).flat(1);
        }

        setTopics(topics);
    };

    const getFormatChips = (queryFormat?: string | string[]) => {
        let formats: IListFormat[];
        if (!queryFormat) {
            formats = [];
        } else if (typeof queryFormat === "string") {
            formats = [formatsData[formatsData.findIndex((element) => element.iri === queryFormat)]];
        } else {
            formats = queryFormat.map((el) => formatsData.filter((e) => e.iri === el)).flat(1);
        }

        setFormats(formats);
    };

    const getKeys = (queryKey?: string | string[]) => {
        let keywords: IListKeyword[];
        if (!queryKey) {
            keywords = [];
        } else if (typeof queryKey === "string") {
            keywords = [keywordsData[keywordsData.findIndex((element) => element.label === queryKey)]];
        } else {
            keywords = queryKey.map((el) => keywordsData.filter((e) => e.label === el)).flat(1);
        }

        setKeys(keywords);
    };

    const getStatuses = (queryStatus?: string | string[]) => {
        let statuses: IStatus[];
        if (!queryStatus) {
            statuses = [];
        } else if (typeof queryStatus === "string") {
            statuses = [statusesData[statusesData.findIndex((element) => element.label === queryStatus)]];
        } else {
            statuses = queryStatus.map((el) => statusesData.filter((e) => e.label === el)).flat(1);
        }

        setStatuses(statuses);
    };

    useEffect(() => {
        getPublisherChip(publisherIri);
        getTopicChips(urlFilter.themeIris);
        getKeys(urlFilter.keywords);
        getFormatChips(urlFilter.formatIris);
        getStatuses(urlFilter.status);
    }, [publisherIri, themeIrisRef.current, keywordsRef.current, formatIrisRef.current]);

    return { topics, publishers, keys, formats, statuses };
};
