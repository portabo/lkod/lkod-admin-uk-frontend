import { useRouter } from "next/navigation";
import { FC } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading/index";
import styles from "@/styles/SignOut.module.scss";

interface Props {
    massage: string;
    pageLabel: string;
}
const StatusPage: FC<Props> = ({ massage, pageLabel }) => {
    const router = useRouter();
    const handleClick = () => {
        router.push("/login");
    };
    return (
        <div className={styles["acsd-container"]}>
            <Heading tag={`h1`} className={styles["acsd-title"]}>
                {pageLabel}
            </Heading>
            <p> {massage}</p>
            <Button
                className={styles["acsd-button"]}
                color="secondary"
                type="submit"
                label={"Zpět na hlavní stranku"}
                onClick={handleClick}
            />
        </div>
    );
};

export default StatusPage;
