"use client";
import React, { createContext, useContext } from "react";

import { IFormats, IKeywords, IPublisher, IStatus, IThemes } from "../(types)/LookupTypes";

type DataContextType = {
    publishersPromise: Promise<IPublisher[]>;
    themesPromise: Promise<IThemes[]>;
    keywordsPromise: Promise<IKeywords[]>;
    formatsPromise: Promise<IFormats[]>;
    statusesPromise: Promise<IStatus[]>;
};

const DataContext = createContext<DataContextType | null>(null);

export const useStatusesPromise = () => {
    const statuses = useContext(DataContext)?.statusesPromise;
    if (!statuses) {
        throw new Error("DataContext (statusesPromise) is not initialized");
    }
    return statuses;
};

export const usePublishersPromise = () => {
    const publishers = useContext(DataContext)?.publishersPromise;
    if (!publishers) {
        throw new Error("DataContext (publishersPromise) is not initialized");
    }
    return publishers;
};

export const useThemesPromise = () => {
    const topics = useContext(DataContext)?.themesPromise;
    if (!topics) {
        throw new Error("DataContext (themesPromise) is not initialized");
    }
    return topics;
};

export const useKeywordsPromise = () => {
    const keywords = useContext(DataContext)?.keywordsPromise;
    if (!keywords) {
        throw new Error("DataContext (keywordsPromise) is not initialized");
    }
    return keywords;
};

export const useFormatsPromise = () => {
    const formats = useContext(DataContext)?.formatsPromise;
    if (!formats) {
        throw new Error("DataContext (formatsPromise) is not initialized");
    }
    return formats;
};

const DataProvider = ({
    children,
    publishersPromise,
    themesPromise,
    keywordsPromise,
    formatsPromise,
    statusesPromise,
}: {
    children: React.ReactNode;
    publishersPromise: Promise<IPublisher[]>;
    themesPromise: Promise<IThemes[]>;
    keywordsPromise: Promise<IKeywords[]>;
    formatsPromise: Promise<IFormats[]>;
    statusesPromise: Promise<IStatus[]>;
}) => {
    return (
        <DataContext.Provider value={{ statusesPromise, publishersPromise, themesPromise, keywordsPromise, formatsPromise }}>
            {children}
        </DataContext.Provider>
    );
};

export default DataProvider;
