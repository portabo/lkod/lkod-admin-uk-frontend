import { useEffect, useRef, useState } from "react";

export const useComponentVisible = (initialIsVisible: boolean, onOutside: () => void) => {
    const [isComponentVisible, setIsComponentVisible] = useState(initialIsVisible);
    const ref = useRef<HTMLDivElement>(null);

    const handleClickOutside = ({ target }: MouseEvent) => {
        if (ref.current && !ref.current.contains(target as Node)) {
            setIsComponentVisible(false);
            onOutside();
        }
    };

    useEffect(() => {
        document.addEventListener("click", handleClickOutside, true);
        return () => {
            document.removeEventListener("click", handleClickOutside, true);
        };
    });

    return { ref, isComponentVisible, setIsComponentVisible };
};
