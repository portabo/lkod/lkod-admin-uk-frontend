/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,

    swcMinify: true,
    images: {
        remotePatterns: [
            { protocol: "https", hostname: "rabingolemio.blob.core.windows.net" },
            { protocol: "https", hostname: "golemgolemio.blob.core.windows.net" },
            { protocol: "https", hostname: "img.icons8.com" },
            { protocol: "https", hostname: "publications.europa.eu" },
        ],
        minimumCacheTTL: 120,
        deviceSizes: [828, 1200],
        formats: ["image/webp"],
    },
    i18n: {
        locales: ["cs-CZ"],
        defaultLocale: "cs-CZ",
    },
    logging: {
        fetches: {
            fullUrl: true,
        },
    },

    async redirects() {
        return [
            {
                source: "/login",
                has: [
                    {
                        type: "cookie",
                        key: "next-auth.session-token",
                        value: "true",
                    },
                ],
                permanent: true,
                destination: "/",
            },
        ];
    },
};

module.exports = nextConfig;
