import React, { Suspense } from "react";

import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { Spinner } from "@/components/Spinner";
import { SearchForm } from "@/modules/search/SearchForm";
import text from "@/textContent/cs.json";
import AddUserButton from "@/users/(components)/AddUserButton";
import UsersResults from "@/users/(components)/UsersResults";

import styles from "./Users.module.scss";

export type UsersProps = {
    searchParams: {
        page: string;
        perpage: string;
        search: string;
    };
};

const Users = async ({ searchParams }: UsersProps) => {
    return (
        <Container>
            <div className={styles["top-container"]}>
                <Heading tag={`h1`}>{text.users.users}</Heading>
                <AddUserButton />
            </div>
            <SearchForm label={text.userSearchString.title} placeholder={text.userSearchString.placeholderText} fullWidth />
            <div className={styles["users-container"]}>
                <Suspense fallback={<Spinner />}>
                    <UsersResults searchParams={searchParams} />
                </Suspense>
            </div>
        </Container>
    );
};

export default Users;
