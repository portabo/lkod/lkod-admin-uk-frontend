import { FC } from "react";

import ResetPasswordToken from "./form";

interface IProps {
    params: {
        token: string;
    };
}

const ResetPasswordPage: FC<IProps> = ({ params }: IProps) => {
    return (
        <>
            <ResetPasswordToken token={params.token} />
        </>
    );
};

export default ResetPasswordPage;
