import React, { DetailedHTMLProps, FC, InputHTMLAttributes } from "react";

import styles from "./TextField.module.scss";

interface ITextField extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    hideLabel?: boolean;
}
const TextField: FC<ITextField> = ({
    children,
    placeholder,
    label,
    hideLabel,
    type = "text",
    value,
    onChange,
    onClick,
    autoComplete,
    ...restProps
}) => {
    return (
        <>
            <label className={styles["label"]}>
                {hideLabel ?? label}
                <span className={styles["input-wrapper"]}>
                    <input
                        className={styles["input-area"]}
                        placeholder={placeholder}
                        value={value}
                        onChange={onChange}
                        type={type}
                        onClick={onClick}
                        autoComplete={autoComplete}
                        aria-label={label}
                        {...restProps}
                    />
                    <span className={styles["input-icon"]}>{children}</span>
                </span>
            </label>
        </>
    );
};

export default TextField;
