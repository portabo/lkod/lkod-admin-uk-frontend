import Image, { StaticImageData } from "next/image";
import { FC } from "react";

import styles from "../Instructions.module.scss";

export const InstructionImage: FC<{
    src: StaticImageData;
    alt: string;
    size: "small" | "large";
    onClick?: () => void;
    clickable?: boolean;
}> = ({ src, alt, size, onClick, clickable = false }) => {
    const className =
        size === "small" ? styles["instruction-lkod__image-frame-small"] : styles["instruction-lkod__image-frame-large"];

    return (
        <div className={`${className} `} onClick={clickable ? onClick : undefined}>
            <Image
                src={src}
                alt={alt}
                width={3836}
                height={2098}
                quality={100}
                unoptimized
                className={size === "small" ? styles["image-cover"] : styles["image-contain"]}
            />
        </div>
    );
};
