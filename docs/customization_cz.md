# Uživatelská konfigurace Lokálního katalogu otevřených dat (Admin)

## Logo a přilehlý text

### Hlavní titulek

- text změníte v souboru `src/locales/translation/cs.json` sekce `configurable/siteTitle`

### Kde měnit obrázky a loga?

#### Favicon

- favicon: složka `public/`
  - nový favicon si můžete vygenerovat zdarma například na `https://favicon.io/` a poté vygenerované soubory nahrát do výše uvedené složky
  - pokud ikonu nezměníte, stránka bude mít v tabu prohlížeče ikonu Hl. m. P.

#### Logo

- logo: soubor `public/assets/images/logos/logoOwner.svg`
- text vedle loga změníte v souboru `src/locales/translation/cs.json` sekce `configurable/logoText`
- komponenta `src/common/components/configurable/Logo/index.tsx`
  - pokud pojmenujete obrázek stejně, zde nic neměníte

## Footer

### Odkazy na sociální sítě

- odkazy na sociální sítě změníte v souboru `src/common/components/Footer/Socials/index.tsx`

### Odkaz z administrace na katalog (v případě použití modulu katalogu i administrace)

- odkaz na váš web katalogu nastavte v ENV proměnné `CATALOG_URL`
- v případě, že používáte pouze modul administrace, deaktivujte prosím odkaz _Do katalogu_ pomocí ENV proměnné `SHOW_CATALOG_LINK=false`
-  v případě, že váš web katalogu nepodporuje připojení k FTP/S3, deaktivujte prosím tlačítko _Soubory_ pomocí ENV proměnné `FILES_BUTTON_DISABLED=true`

## Kde měnit barvy?

- použité barvy lze měnit v souboru `src/styles/_variables.scss`
