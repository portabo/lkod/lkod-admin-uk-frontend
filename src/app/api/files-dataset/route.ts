import { NextRequest } from "next/server";

import { ApiClient } from "@/(services)/api-client";
import { getLogger } from "@/logging/logger";

import { RequestInit } from "../../(services)/client/datasets-service-client";
import { getErrorMessage } from "../../(utils)/getErrorMessage";

export const dynamic = "force-dynamic";
const logger = getLogger("files-datasets-route");

export async function POST(request: NextRequest) {
    try {
        const clientRouteRequestParams: RequestInit = await request.json();
        const { method, requestUrl, body, headers } = clientRouteRequestParams;

        logger.debug({
            provider: "server",
            component: "Route Handler files-dataset",
            message: `Received request for ${method} ${requestUrl}`,
            requestBody: body ? "Body included" : "No body",
            requestHeaders: headers || "No headers",
        });

        const res = await ApiClient.request<Blob>(method, requestUrl, body, headers);

        logger.debug({
            provider: "server",
            component: "Route Handler-files-dataset",
            message: `Request completed for ${method} ${requestUrl}`,
            status: res.status,
        });

        if (res.status >= 200 && res.status < 300) {
            logger.debug({
                provider: "server",
                component: "Route Handler files-dataset",
                message: `Response successful for ${method} ${requestUrl}`,
                status: res.status,
            });
            return new Response(res.data?.stream(), {
                status: 200,
                statusText: "OK",
                headers: {
                    "Content-Type": "text/csv",
                    "Content-Disposition": `attachment; filename=datasetsList.csv`,
                },
            });
        } else {
            const errorMessage = `Failed to fetch data from ${requestUrl}. Status: ${res.status}`;
            logger.error({
                provider: "server",
                component: "Route Handler files-dataset",
                message: errorMessage,
            });
            return new Response(JSON.stringify({ success: false, error: errorMessage }), {
                status: res.status,
            });
        }
    } catch (err) {
        logger.error({
            provider: "server",
            component: "Route Handler files-dataset",
            status: err.status,
            message: getErrorMessage(err),
        });

        return new Response(JSON.stringify({ success: false, error: getErrorMessage(err) }), {
            status: err.status,
        });
    }
}
