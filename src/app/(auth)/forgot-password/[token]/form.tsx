"use client";
import type { NextPage } from "next";
import React, { ChangeEvent, SyntheticEvent, useState } from "react";

import Button from "@/components/Button";
import { Card } from "@/components/Card";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import { EyeIcon } from "@/components/icons/Eye";
import { EyeSlashIcon } from "@/components/icons/Eye-slash";
import StatusPage from "@/components/StatusPage";
import TextField from "@/components/TextField";
import { ApiClient } from "@/services/initApi";
import styles from "@/styles/Login.module.scss";
import text from "@/textContent/cs.json";

const PAGE_LABEL = "Vytvořte si nové heslo";

interface IProps {
    token: string;
}

interface Error {
    status?: number;
    code?: number;
}

const ResetPasswordToken: NextPage<IProps> = (props: IProps) => {
    const [typeNewPassword, setTypeNewPassword] = useState("password");
    const [typePasswordRepeat, setTypePasswordRepeat] = useState("password");
    const [newPassword, setNewPassword] = useState("");
    const [passwordRepeat, setPasswordRepeat] = useState("");
    const [resetSuccess, setResetSuccess] = useState<string | null>(null);
    const [resetError, setResetError] = useState("");
    const { authorizationApi } = ApiClient;

    const onSubmit = async (e: SyntheticEvent) => {
        e.preventDefault();
        if (newPassword !== passwordRepeat) {
            setResetError("Hesla se neshodují");
            return;
        }

        try {
            const response = await authorizationApi.resetPassword({
                token: props.token,
                password: newPassword,
            });
            if (response.status >= 200 && response.status <= 299) {
                setResetSuccess(response.status.toString());
            } else {
                throw { status: response.status };
            }
            // eslint-disable-next-line
        } catch (error) {
            setResetSuccess(null);
            setResetError(getErrorKey(error.response));
        }
    };

    const getErrorKey = (reason: Error) => {
        switch (reason?.status) {
            case 400:
            case 401:
                return "Nesprávné přihlašovací údaje";
            default:
                return "Došlo k neznámé chybě";
        }
    };

    return (
        <Container className={`${styles["login-container"]}`}>
            {resetSuccess ? (
                <StatusPage pageLabel={text.passwordChanged} massage={text.passwordChangedMessage} />
            ) : (
                <div className={`${styles["container"]}`}>
                    <Card className={`${styles["login-box"]}`}>
                        <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                            {PAGE_LABEL}
                        </Heading>
                        <form className={`${styles["form-wrapper"]}`} onSubmit={onSubmit}>
                            <TextField
                                name="password"
                                type={typeNewPassword}
                                placeholder="Zadejte nové heslo"
                                label="Nové heslo"
                                value={newPassword}
                                onChange={(e: ChangeEvent) => setNewPassword((e.target as HTMLInputElement).value)}
                            >
                                {typeNewPassword === "password" ? (
                                    <EyeIcon color={`primary-light`} onClick={() => setTypeNewPassword("text")} />
                                ) : (
                                    <EyeSlashIcon color={`primary-light`} onClick={() => setTypeNewPassword("password")} />
                                )}
                            </TextField>
                            <TextField
                                name="password"
                                type={typePasswordRepeat}
                                placeholder="Opakujte nové heslo"
                                label="Opakujte nové heslo"
                                value={passwordRepeat}
                                onChange={(e: ChangeEvent) => setPasswordRepeat((e.target as HTMLInputElement).value)}
                            >
                                {typePasswordRepeat === "password" ? (
                                    <EyeIcon color={`primary-light`} onClick={() => setTypePasswordRepeat("text")} />
                                ) : (
                                    <EyeSlashIcon color={`primary-light`} onClick={() => setTypePasswordRepeat("password")} />
                                )}
                            </TextField>
                            <div className={`${styles["input-button"]}`}>
                                <Button color="secondary" type="submit" label={"Změnit heslo"} />
                                <div className={`${styles["error"]}`}>{resetError}</div>
                            </div>
                        </form>
                    </Card>
                </div>
            )}
        </Container>
    );
};

export default ResetPasswordToken;
