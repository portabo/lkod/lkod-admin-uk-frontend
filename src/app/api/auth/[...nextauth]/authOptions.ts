import { type Session, type User } from "next-auth";
import { NextAuthOptions } from "next-auth/index";
import { JWT } from "next-auth/jwt";
import CredentialsProvider from "next-auth/providers/credentials";
import { config } from "src/config";

import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/root/app/(utils)/getErrorMessage";
const logger = getLogger("auth");

export const authOptions: NextAuthOptions = {
    providers: [
        CredentialsProvider({
            name: "LKOD auth",
            id: "lkod-auth",
            credentials: {
                email: { label: "Email", type: "text", placeholder: "username@gmail.com" },
                password: { label: "Password", type: "password" },
            },
            // eslint-disable-next-line
            async authorize(credentials: Record<"email" | "password", string> | undefined): Promise<any> {
                if (!credentials?.email || !credentials.password) {
                    logger.warn({
                        provider: "server",
                        component: "authOptions-next-auth",
                        action: "authorize",
                        message: `Missing credentials during authorization. Missing fields: ${JSON.stringify({
                            email: credentials?.email,
                            password: credentials?.password ? "[REDACTED]" : "missing",
                        })}`,
                    });
                    return null;
                }

                try {
                    const response = await fetch(`${config.BACKEND_URL}/auth/login`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify(credentials),
                    });

                    if (!response.ok) {
                        // eslint-disable-next-line max-len
                        const errorMessage = `Authorization failed - status: ${response.status ? response.status : response}`;
                        logger.error({
                            provider: "server",
                            component: "authOptions",
                            action: "authorize",
                            status: response.status,
                            message: errorMessage,
                        });
                        return null;
                    }

                    const responseData = await response.json();

                    if (responseData.accessToken) {
                        return {
                            email: credentials.email,
                            token: responseData.accessToken,
                            defaultPassword: responseData.hasDefaultPassword,
                            role: responseData.role,
                        };
                    } else {
                        logger.warn({
                            provider: "server",
                            component: "authOptions-next-auth",
                            action: "authorize",
                            message: `No accessToken, status: ${response.statusText || response.status}`,
                        });
                        return null;
                    }
                } catch (error) {
                    logger.error({
                        provider: "server",
                        component: "authOptions-next-auth",
                        action: "authorize",
                        message: `Error ${error.message}`,
                        error: getErrorMessage(error),
                    });
                    return null;
                }
            },
        }),
    ],

    callbacks: {
        jwt: ({ token, user }: { token: JWT; user: User }) => {
            if (user) {
                token.token = user.token;
                token.defaultPassword = user.defaultPassword;
                token.role = user.role;
            }

            return token;
        },

        session: ({ session, token }: { session: Session; token: JWT }) => {
            return {
                ...session,
                token: token.token,
                isLoggedIn: true,
                defaultPassword: token.defaultPassword,
                user: { ...session.user, role: token.role },
            };
        },
    },

    secret: config.NEXTAUTH_SECRET,
    jwt: {
        secret: config.NEXTAUTH_SECRET,
    },
    pages: {
        signIn: "/login",
        signOut: "/signout",
    },
};
