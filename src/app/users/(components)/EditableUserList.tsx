"use client";
import { useRouter } from "next/navigation";
import React, { useState } from "react";

import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
import UserIcon from "@/components/icons/UserIcon";
import { NotificationType, useNotifications } from "@/hooks/useNotifications";
import UserList, { UserListAction } from "@/modules/users/UserList/UserList";
import { ResponseType } from "@/services/api-client";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import AddUserDialog from "@/users/(components)/AddUserDialog";
import { deleteUser } from "@/users/actions";

import { getLogger } from "../../(logging)/logger";
import { getErrorMessage } from "../../(utils)/getErrorMessage";
const logger = getLogger("OrganizationMembers");

export const EditableUserList = ({ users, currentUserId }: { users: User[]; currentUserId: number }) => {
    const { addNotification } = useNotifications();
    const router = useRouter();
    const [toUpdate, setToUpdate] = useState<User | undefined>(undefined);
    const [toDelete, setToDelete] = useState<User | undefined>(undefined);

    const refreshOrThrow = (response: ResponseType<void>) => {
        if (response.status === 204) {
            router.refresh();
        } else {
            throw { status: response.status };
        }
    };

    const deleteUserLocal = async (userId: number) => {
        try {
            const response = await deleteUser(userId);
            refreshOrThrow(response);
        } catch (error) {
            logger.error({
                component: "OrganizationMembers",
                action: "deleteUserLocal",
                message: getErrorMessage(error),
                status: error.status,
            });
            addNotification("Uživatele se nepodařilo odstranit", NotificationType.ERROR);
        }
    };

    const onUserAction = async (user: User, action: UserListAction) => {
        if (action === "remove") {
            setToDelete(user);
        } else if (action === "edit") {
            setToUpdate(user);
        }
    };

    return (
        <>
            <UserList users={users} action={"manage"} onClick={onUserAction} currentUserId={currentUserId} />
            <AddUserDialog isOpen={!!toUpdate} toUpdate={toUpdate} closeDialog={() => setToUpdate(undefined)} />
            <DeleteDialog
                isOpen={!!toDelete}
                name={toDelete?.name}
                titleIcon={<UserIcon color="primary" />}
                titleText={text.users.delete.prompt}
                subtitleText={text.users.delete.subprompt}
                deleteButtonText={text.users.delete.action}
                onConfirm={() => deleteUserLocal((toDelete as User).id)}
                closeDialog={() => setToDelete(undefined)}
            />
        </>
    );
};
