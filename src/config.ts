interface Config {
    BACKEND_URL: string;
    EXTERNAL_FORM_URL: string;
    RETURN_URL: string;
    environment: string;
    NEXTAUTH_SECRET: string;
    CATALOG_URL: string;
    SHOW_CATALOG_LINK: string;
    FILES_BUTTON_DISABLED: string;
    NEXT_PUBLIC_LOG_LEVEL: string;
    NEXT_PUBLIC_URL: string;
    NEXTAUTH_URL: string;
}

export const config: Config = {
    BACKEND_URL: process.env.BACKEND_URL,
    EXTERNAL_FORM_URL: process.env.EXTERNAL_FORM_URL,
    RETURN_URL: process.env.RETURN_URL,
    environment: process.env.NODE_ENV,
    NEXTAUTH_SECRET: process.env.NEXTAUTH_SECRET,
    CATALOG_URL: process.env.CATALOG_URL,
    SHOW_CATALOG_LINK: process.env.SHOW_CATALOG_LINK ?? "",
    FILES_BUTTON_DISABLED: process.env.FILES_BUTTON_DISABLED ?? "false",
    NEXT_PUBLIC_LOG_LEVEL: process.env.NEXT_PUBLIC_LOG_LEVEL ?? "",
    NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL ?? "",
    NEXTAUTH_URL: process.env.NEXTAUTH_URL ?? "",
};
