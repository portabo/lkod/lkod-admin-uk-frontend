"use client";
/* eslint-disable @typescript-eslint/no-empty-function */
import { signOut } from "next-auth/react";
import React, { createContext, FC, ReactNode, useCallback, useMemo, useState } from "react";

import { ApiClient } from "@/services/initApi";
import { Organization } from "@/services/organizations-service";

type OrganizationsState = {
    organizations: Organization[];
    isLoading: boolean;
    organizationsError: string | null;
};
type OrganizationsActions = {
    refreshOrganizations: () => void;
};

const defaultState: OrganizationsState = {
    organizations: [],
    isLoading: false,
    organizationsError: null,
};

export const OrganizationsContext = createContext<OrganizationsState & OrganizationsActions>({
    ...defaultState,
    refreshOrganizations: () => {},
});

export const OrganizationsProvider: FC<{ children: ReactNode }> = ({ children }) => {
    const [organizations, setOrganizations] = useState<Organization[]>(defaultState.organizations);
    const [isLoading, setLoading] = useState<boolean>(defaultState.isLoading);
    const [organizationsError, setOrganizationsError] = useState<string | null>(defaultState.organizationsError);

    const { organizationsApi } = ApiClient;

    const refreshOrganizations = useCallback(() => {
        setLoading(true);
        setOrganizationsError(null);
        setOrganizations([]);
        organizationsApi
            .getAllOrganization()
            .then((organizations) => {
                setLoading(false);
                setOrganizations(organizations.data as Organization[]);
            })
            .catch((reason) => {
                setLoading(false);
                if (reason?.status === 401) {
                    signOut();
                    return;
                }

                setOrganizationsError("errors.unknown");
            });
    }, [organizationsApi]);

    const organizationsState = useMemo(() => {
        return {
            organizations,
            isLoading,
            organizationsError: organizationsError && organizationsError.toString(),
            refreshOrganizations,
        };
    }, [organizations, isLoading, organizationsError, refreshOrganizations]);

    return <OrganizationsContext.Provider value={organizationsState}>{children}</OrganizationsContext.Provider>;
};
