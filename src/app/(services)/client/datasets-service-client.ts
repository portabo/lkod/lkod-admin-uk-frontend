/* eslint-disable @typescript-eslint/no-unused-vars */
import { config } from "src/config";

import {
    CreateDatasetRequest,
    CreateSession,
    Dataset,
    DatasetId,
    DeleteFileRequest,
    GetAllFilesRequest,
    UpdateDatasetInput,
} from "@/types/Dataset";

import { CreateSessionRequest } from "../session-service";

export type ResponseType<T> = {
    count: string | null;
    data: T | null;
    status: number;
    error?: string;
};
export interface RequestInit {
    method: "GET" | "POST" | "DELETE" | "PATCH";
    requestUrl: string;
    body?: unknown | FormData;
    headers?: HeadersInit;
}
export interface UploadRequestInit {
    requestUrl?: string;
    body?: unknown | FormData;
    headers?: HeadersInit;
}

class DataServiceClient {
    private apiBase: string;

    constructor() {
        this.apiBase = `${config.NEXT_PUBLIC_URL || ""}/api/`;
    }

    private async fetchData<T>({ method, requestUrl, body, headers }: RequestInit): Promise<T> {
        try {
            const clientRouteRequestParams: RequestInit = {
                method,
                requestUrl,
                body,
                headers,
            };
            const response = await fetch(`${this.apiBase}datasets-service`, {
                method: method,
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(clientRouteRequestParams),
                next: { revalidate: 0 },
                cache: "no-store",
            });
            if (!response.ok) {
                throw new Error(`Failed to fetch ${requestUrl}: ${response.statusText}`);
            }
            const res = await response.json();

            return res.data;
        } catch (error) {
            throw new Error(`Error fetching ${requestUrl}: ${error}`);
        }
    }
    private async fetchDataGet<T>({ requestUrl }: RequestInit): Promise<T> {
        try {
            const response = await fetch(`${this.apiBase}datasets-service?${requestUrl}`, {
                method: "GET",
                next: { revalidate: 0 },
                cache: "no-store",
            });
            if (!response.ok) {
                throw new Error(`Failed to fetch ${requestUrl}: ${response.statusText}`);
            }
            const res = await response.json();
            const result = res;

            return result;
        } catch (error) {
            throw new Error(`Error fetching ${requestUrl}: ${error}`);
        }
    }
    private async fetchDataBlob({ method, requestUrl, body, headers }: RequestInit): Promise<Blob> {
        try {
            const clientRouteRequestParams: RequestInit = {
                method,
                requestUrl,
                body,
                headers,
            };

            const response = await fetch(`${this.apiBase}files-dataset`, {
                method: "POST",
                body: JSON.stringify(clientRouteRequestParams),
                next: { revalidate: 0 },
                cache: "no-store",
            });

            if (!response.ok) {
                throw new Error(`Failed to fetch ${requestUrl}: ${response.statusText}`);
            }
            const result = await response.blob();

            return result;
        } catch (error) {
            throw new Error(`Error fetching ${requestUrl}: ${error}`);
        }
    }

    private transformSessionOutput = (json: CreateSession): CreateSession => {
        if (json === undefined || json === null) {
            return json;
        }
        return {
            id: json.id,
            datasetId: json.datasetId,
            userId: json.userId,
            createdAt: json.createdAt,
            expiresIn: json.expiresIn,
        };
    };

    async getAllFiles(requestParameters: GetAllFilesRequest): Promise<FileList> {
        const path = `datasets/${encodeURIComponent(String(requestParameters.datasetId))}/files`;
        return this.fetchDataGet<FileList>({ method: "GET", requestUrl: path });
    }

    async updateDataset(datasetId: string, updateDatasetInput: UpdateDatasetInput): Promise<Dataset> {
        const path = `datasets/${encodeURIComponent(String(datasetId))}`;
        return this.fetchData<Dataset>({ method: "PATCH", requestUrl: path, body: updateDatasetInput });
    }

    async createDataset(body: CreateDatasetRequest): Promise<Dataset> {
        return this.fetchData<Dataset>({ method: "POST", requestUrl: "datasets", body: body.createDatasetInput });
    }

    async createSession(body: CreateSessionRequest): Promise<CreateSession> {
        const res = await this.fetchData<CreateSession>({
            method: "POST",
            requestUrl: "sessions",
            body: body.createSessionInput,
        });
        return this.transformSessionOutput(res);
    }

    async deleteFile(body: DeleteFileRequest): Promise<void> {
        const path = `datasets/${encodeURIComponent(String(body.datasetId))}/files/${encodeURIComponent(String(body.filename))}`;
        return this.fetchData<void>({ method: "DELETE", requestUrl: path });
    }

    async deleteDataset(body: DatasetId): Promise<ResponseType<unknown>> {
        const path = `datasets/${encodeURIComponent(String(body.datasetId))}`;
        return this.fetchData({ method: "DELETE", requestUrl: path });
    }
    async getDatasetsListFile(): Promise<Blob> {
        const res = await this.fetchDataBlob({
            method: "GET",
            requestUrl: "datasets.csv",
            body: undefined,
            headers: { "Content-Type": "text/csv" },
        });
        return res;
    }
}

const dataServiceClient = new DataServiceClient();
export { dataServiceClient };
