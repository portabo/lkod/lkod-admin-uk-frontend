"use client";
import type { NextPage } from "next";
import { useRouter } from "next/navigation";
import React, { ChangeEvent, SyntheticEvent, useState } from "react";

import Button from "@/components/Button";
import { Card } from "@/components/Card";
import ColorLink from "@/components/ColorLink";
import Container from "@/components/Container";
import { Heading } from "@/components/Heading";
import StatusPage from "@/components/StatusPage";
import TextField from "@/components/TextField";
import { ApiClient } from "@/services/initApi";
import styles from "@/styles/Login.module.scss";
import text from "@/textContent/cs.json";

const PAGE_LABEL = "Email pro resetování hesla";

const ForgotPassword: NextPage = () => {
    const [email, setEmail] = useState("");
    const [resetSuccess, setResetSuccess] = useState<string | null>(null);
    const [resetError, setResetError] = useState("");
    const router = useRouter();
    const { authorizationApi } = ApiClient;
    let sendingEmail = email;
    const index = sendingEmail.lastIndexOf("@");
    for (let i = 2; i < index; i++) {
        sendingEmail = sendingEmail.replace(sendingEmail[i], "*");
    }

    const massage = `Na ověřenou e-mailovou adresu ${sendingEmail} jsme odeslali odkaz, pomocí kterého dokončíte obnovu hesla`;
    const pageLabel = "E-mail pro obnovu hesla byl odeslán";

    const onSubmit = async (e: SyntheticEvent) => {
        e.preventDefault();
        try {
            const response = await authorizationApi.forgotPassword({ email });
            if (response.status === 202) {
                setResetSuccess(response.status.toString());
            } else {
                throw { status: response.status };
            }
            // eslint-disable-next-line
        } catch (error: any) {
            setResetError(getErrorKey(error.status));
            setResetSuccess(null);
        }
    };

    const getErrorKey = (reason: number | string) => {
        switch (reason) {
            case 400:
                return `${text.errors.emptyfields}`;
            case 401:
                return `${text.errors.invalidLogin}`;
            default:
                return `${text.errors.unknown}`;
        }
    };

    return (
        <Container className={`${styles["login-container"]}`}>
            {resetSuccess ? (
                <StatusPage pageLabel={pageLabel} massage={massage} />
            ) : (
                <>
                    <div className={`${styles["login-link-back"]}`}>
                        <ColorLink linkText={text.back} onClick={() => router.back()} direction={`left`} start />
                    </div>
                    <div className={`${styles["container"]}`}>
                        <Card className={`${styles["login-box"]}`}>
                            <Heading tag={`h1`} type={`h3`} className={`${styles["login-title"]}`}>
                                {PAGE_LABEL}
                            </Heading>
                            <div className={`${styles["login-subtitle"]}`}>Napište email, který jste uvedli při registraci</div>

                            <form className={`${styles["form-wrapper"]}`} onSubmit={onSubmit}>
                                <TextField
                                    name="email"
                                    type="email"
                                    placeholder={text.login.inputEmail}
                                    label="E-mail"
                                    value={email}
                                    onChange={(e: ChangeEvent) => setEmail((e.target as HTMLInputElement).value)}
                                />
                                <div className={`${styles["input-button"]}`}>
                                    <Button color="secondary" type="submit" label={text.send} />
                                    <div className={`${styles["error"]}`}>{resetError}</div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </>
            )}
        </Container>
    );
};

export default ForgotPassword;
