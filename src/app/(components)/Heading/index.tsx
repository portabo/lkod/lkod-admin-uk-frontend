/* eslint-disable react/display-name */
import { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import styles from "./Heading.module.scss";

type Tags = "h1" | "h2" | "h3" | "h4" | "h5" | "h6";

type Type = Tags;

type Colors = "secondary" | "primary" | "gray" | "gray-light" | "white";

interface HeadingProps extends DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement> {
    tag?: Tags;
    type?: Type;
    color?: Colors;
    className?: string;
}

export const Heading = forwardRef<HTMLHeadingElement, HeadingProps>(
    ({ tag = "h1", type = tag, color, className, ...restProps }, ref) => {
        const Tag = tag;
        return (
            <Tag
                className={`${styles.heading} ${styles[type]} ${styles[`heading_color_${color}`]} ${className ?? ""}`}
                {...restProps}
                ref={ref}
            />
        );
    }
);
