import { User } from "@/services/users-service";

import { ApiClient, ResponseType } from "./api-client";

export interface Organization extends OrganizationFields {
    hasArcGisFeed?: boolean;
    id: number;
}

export interface OrganizationFields {
    description: string | null;
    identificationNumber: string;
    logo: string | null;
    name: string;
    slug: string;
    arcGisFeed?: string | null;
}

export class OrganizationsService {
    async getAllOrganization(): Promise<ResponseType<Organization[]>> {
        return await ApiClient.get<Organization[]>("organizations");
    }

    async addOrganization(body: OrganizationFields): Promise<ResponseType<Organization>> {
        return await ApiClient.post<Organization>(`organizations`, body);
    }

    async updateOrganization(id: number, body: OrganizationFields): Promise<ResponseType<Organization>> {
        return await ApiClient.patch<Organization>(`organizations/${id}`, body);
    }

    async deleteOrganization(id: number): Promise<ResponseType<void>> {
        return await ApiClient.delete(`organizations/${id}`);
    }

    async getOrganizationMembers(id: number): Promise<ResponseType<User[]>> {
        return await ApiClient.get<User[]>(`organizations/${id}/members`);
    }

    async addOrganizationMember(organizationId: number, userId: number): Promise<ResponseType<void>> {
        return await ApiClient.post<void>(`organizations/${organizationId}/members/${userId}`, null);
    }

    async removeOrganizationMember(organizationId: number, userId: number): Promise<ResponseType<void>> {
        return await ApiClient.delete<void>(`organizations/${organizationId}/members/${userId}`);
    }
}
