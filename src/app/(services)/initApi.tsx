import { AuthorizationServices } from "@/services/authorization-service";
import { DatasetsServices } from "@/services/datasets-service";
import { FormDataService } from "@/services/form-data-service";
import { OrganizationsService } from "@/services/organizations-service";
import { SessionsService } from "@/services/session-service";
import { StatusService } from "@/services/status-service";
import { UsersService } from "@/services/users-service";

interface IApiClient {
    datasetsApi: DatasetsServices;
    authorizationApi: AuthorizationServices;
    formDataApi: FormDataService;
    organizationsApi: OrganizationsService;
    sessionsApi: SessionsService;
    statusApi: StatusService;
    usersApi: UsersService;
}

const initApis = (): IApiClient => {
    return {
        authorizationApi: new AuthorizationServices(),
        datasetsApi: new DatasetsServices(),
        formDataApi: new FormDataService(),
        organizationsApi: new OrganizationsService(),
        sessionsApi: new SessionsService(),
        statusApi: new StatusService(),
        usersApi: new UsersService(),
    };
};

export const ApiClient = initApis();
