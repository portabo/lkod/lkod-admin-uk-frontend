import React, { DetailedHTMLProps, HTMLAttributes, useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";

import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import { ButtonReset } from "../ButtonReset";
import styles from "./Modal.module.scss";

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    show: boolean;
    onClose: () => void;
    label?: string;
    headerText?: string;
    headerWarning?: string;
}

export const Modal = ({ show, onClose, children, label, headerText, headerWarning, className, ...restProps }: Props) => {
    const [isBrowser, setIsBrowser] = useState(false);
    const modalRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        setIsBrowser(true);
    }, []);

    useEffect(() => {
        function keyListener(e: KeyboardEvent) {
            const listener = keyListenersMap.get(e.keyCode);
            return listener && listener(e);
        }
        document.addEventListener("keydown", keyListener);

        return () => document.removeEventListener("keydown", keyListener);
    });

    const handleTabKey = (e: KeyboardEvent) => {
        if (modalRef.current) {
            const focusableModalElements = modalRef.current.querySelectorAll(
                'a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select'
            );
            const firstElement = focusableModalElements[0];
            const lastElement = focusableModalElements[focusableModalElements.length - 1];
            const activeElements = Array.from(focusableModalElements).filter((e) => document.activeElement === e);

            if (activeElements.length === 0) {
                (firstElement as HTMLElement).focus();
                e.preventDefault();
            }

            if (!e.shiftKey && document.activeElement === lastElement) {
                (firstElement as HTMLElement).focus();
                e.preventDefault();
            }

            if (e.shiftKey && document.activeElement === firstElement) {
                (lastElement as HTMLElement).focus();
                e.preventDefault();
            }
        }
    };

    const keyListenersMap = new Map([
        [27, onClose],
        [9, handleTabKey],
    ]);

    const modalContent = show ? (
        <div aria-hidden={!show} aria-describedby="modalDescription">
            <div className={styles["overlay"]} onClick={() => onClose()}></div>
            <section className={`${styles["modal"]} ${className}`} ref={modalRef} {...restProps}>
                <div className={styles["screenreader-text"]} id="modalDescription">
                    Toto je modální okno. Okno zavřete pomocí tlačítka Zavřít nebo klávesou Escape.
                </div>
                <div className={styles["modal__header"]}>
                    <ButtonReset
                        label={text.close}
                        hideLabel
                        onClick={() => onClose()}
                        color="primary"
                        className={styles["modal__close-button"]}
                    />
                    <div className={styles["modal__title"]}>
                        <Heading tag={`h4`}>
                            {label}
                            {headerText && <span className={styles["modal__header-subtext"]}>{headerText}</span>}
                        </Heading>
                        {headerWarning && <span className={styles["modal__header-warning"]}>{headerWarning}</span>}
                    </div>
                </div>
                <div className={styles["modal__body"]}>{children}</div>
            </section>
        </div>
    ) : null;

    if (isBrowser) {
        return ReactDOM.createPortal(modalContent, document.getElementById("modal-root") as HTMLElement);
    } else {
        return null;
    }
};
