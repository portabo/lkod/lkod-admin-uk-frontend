import { handleRequest, handleRequestGet } from "../../(utils)/handleRequest";

export const dynamic = "force-dynamic";

export async function GET(request: Request) {
    const response = await handleRequestGet(request);
    return response;
}

export async function POST(request: Request) {
    const response = await handleRequest(request);
    return response;
}

export async function DELETE(request: Request) {
    const response = await handleRequest(request);
    return response;
}

export async function PATCH(request: Request) {
    const response = await handleRequest(request);
    return response;
}
