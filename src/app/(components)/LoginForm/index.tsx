"use client";
import type { NextPage } from "next";
import Link from "next/link";
import { useRouter, useSearchParams } from "next/navigation";
import { getSession, signIn } from "next-auth/react";
import React, { ChangeEvent, SyntheticEvent, useState } from "react";

import Button from "@/components/Button";
import { EyeIcon } from "@/components/icons/Eye";
import { EyeSlashIcon } from "@/components/icons/Eye-slash";
import TextField from "@/components/TextField";
import text from "@/textContent/cs.json";

import styles from "./LoginForm.module.scss";

const LoginForm: NextPage = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loginError, setLoginError] = useState("");
    const [inputType, setInputType] = useState("password");
    const fromUrl = useSearchParams().get("from");

    const router = useRouter();

    const handleSubmit = async (e: SyntheticEvent) => {
        e.preventDefault();
        setLoginError("");
        const response = await signIn("lkod-auth", {
            email: email,
            password: password,
            redirect: false,
        });
        if (response?.ok) {
            window.location.replace(fromUrl ?? "/");
        }

        const session = await getSession();
        if (response?.error) {
            setLoginError(getErrorKey(response));
            return;
        }
        if (session?.defaultPassword === true) {
            router.push("/change-password");
            return;
        }
    };

    interface Error {
        status?: number;
        code?: number;
    }

    const getErrorKey = (reason: Error) => {
        switch (reason?.status) {
            case 400:
            case 401:
                return text.errors.invalidLogin;
            default:
                return text.errors.unknown;
        }
    };

    return (
        <form className={`${styles["form-wrapper"]}`} onSubmit={handleSubmit}>
            <TextField
                name="email"
                type="email"
                placeholder={text.login.inputEmail}
                label="E-mail"
                autoComplete={"email"}
                value={email}
                onChange={(e: ChangeEvent) => setEmail((e.target as HTMLInputElement).value)}
            />
            <TextField
                name="password"
                type={inputType}
                placeholder={text.login.inputPassword}
                label="Heslo"
                value={password}
                autoComplete={"current-password"}
                onChange={(e: ChangeEvent) => setPassword((e.target as HTMLInputElement).value)}
            >
                {inputType === "password" ? (
                    <EyeIcon color={`primary-light`} onClick={() => setInputType("text")} />
                ) : (
                    <EyeSlashIcon color={`primary-light`} onClick={() => setInputType("password")} />
                )}
            </TextField>
            <div className={`${styles["input-link"]}`}>
                <Link href="/forgot-password">{text.password.forgottenPassword}</Link>
            </div>
            <div className={`${styles["input-button"]}`}>
                <Button color="secondary" type="submit" label={text.login.logMeIn} />
                <div className={`${styles["error"]}`}>{loginError}</div>
            </div>
        </form>
    );
};

export default LoginForm;
