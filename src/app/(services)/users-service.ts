import { UserRole } from "@/services/authorization-service";

import { ApiClient, ResponseType } from "./api-client";

export interface User extends UserFields {
    id: number;
    password?: string | null;
}

export interface UserFields {
    email: string;
    name: string;
    role: UserRole;
}

export class UsersService {
    async getAllUsers(): Promise<ResponseType<User[]>> {
        return ApiClient.get("users");
    }

    async getCurrentUser(): Promise<ResponseType<User>> {
        return ApiClient.get("users/me");
    }

    async addUser(body: UserFields): Promise<ResponseType<User>> {
        return await ApiClient.post<User>(`users`, body);
    }

    async updateUser(userId: number, body: UserFields): Promise<ResponseType<User>> {
        return await ApiClient.patch<User>(`users/${userId}`, body);
    }

    async deleteUser(userId: number): Promise<ResponseType<void>> {
        return await ApiClient.delete<void>(`users/${userId}`);
    }
}
