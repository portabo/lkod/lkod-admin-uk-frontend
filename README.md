# Golemio LKOD Frontend

Component of the [Golemio LKOD](https://gitlab.com/operator-ict/golemio/lkod/lkod-general).

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Developed by http://operatorict.cz

![you are here](./docs/assets/lkod_achitecture-frontend.png)


## Local Installation

### Prerequisites

- NodeJS v18 (https://nodejs.org)
- npm (https://www.npmjs.com/)
- TypeScript (https://www.typescriptlang.org/)

### Installation

Install all prerequisites.

Install all dependencies using command:
```
npm install
```

from the application's root directory.

### Build & Run

Running the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Basic customization

For basic site customization please read [customization_cz.md](./docs/customization_cz.md).
