declare namespace NodeJS {
    export interface ProcessEnv {
        readonly EXTERNAL_FORM_URL: string;
        readonly BACKEND_URL: string;
        readonly RETURN_URL: string;
        readonly ENVIROMENT: typeof process.env.NODE_ENV;
        readonly NEXTAUTH_SECRET: string;
        readonly CATALOG_URL: string;
        readonly NEXTAUTH_URL: string;
    }
}
