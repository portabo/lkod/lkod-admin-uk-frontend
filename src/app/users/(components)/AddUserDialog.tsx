import { useRouter } from "next/navigation";
import React from "react";

import { Modal } from "@/components/Modal";
import { User } from "@/services/users-service";
import UserForm from "@/users/(components)/UserForm";

interface AddUserDialogProps {
    toUpdate?: User;
    isOpen: boolean;
    closeDialog: () => void;
}

const AddUserDialog = (props: AddUserDialogProps) => {
    const router = useRouter();
    return (
        <Modal
            show={props.isOpen}
            onClose={props.closeDialog}
            label={props.toUpdate ? "Upravit uživatele" : "Přidat uživatele"}
            headerText={props.toUpdate ? `${props.toUpdate.name} | ${props.toUpdate.email}` : ""}
            headerWarning={props.toUpdate ? "" : "Odkaz na vytvoření hesla bude zaslán na zadanou emailovou adresu."}
        >
            <UserForm
                toUpdate={props.toUpdate}
                onSaved={() => {
                    router.refresh();
                    props.closeDialog();
                }}
            />
        </Modal>
    );
};

export default AddUserDialog;
