import React, { FC, HTMLAttributes } from "react";
import { DetailedHTMLProps } from "react";

import styles from "./Container.module.scss";

type Props = DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;

const Container: FC<Props> = ({ children }) => {
    return (
        <main className={styles.main}>
            <div className={styles.container}>{children}</div>
        </main>
    );
};

export default Container;
