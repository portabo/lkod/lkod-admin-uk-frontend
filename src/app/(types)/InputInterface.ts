import { InputHTMLAttributes } from "react";

export interface InputElementProps extends InputHTMLAttributes<HTMLInputElement> {
    checked: boolean;
    count?: string;
    disabled?: boolean;
    id: string;
    key?: string;
    label: string;
    onClick: () => void;
}
