"use client";
import { useRouter } from "next/navigation";
import React, { SyntheticEvent, useState } from "react";

import Button from "@/components/Button";
import TextArea from "@/components/TextArea";
import TextField from "@/components/TextField";
import styles from "@/organizations/(components)/OrganizationForm/OrganizationForm.module.scss";
import { addOrganization, updateOrganization } from "@/organizations/actions";
import { ResponseType } from "@/services/api-client";
import { Organization } from "@/services/organizations-service";
import text from "@/textContent/cs.json";

interface OrganizationFormProps {
    toUpdate?: Organization;
}

const OrganizationForm = (props: OrganizationFormProps) => {
    const { toUpdate } = props;
    const router = useRouter();

    const [status, setStatus] = useState<undefined | null | string>(undefined);
    const [fields, setFields] = useState({
        name: toUpdate?.name ?? "",
        identificationNumber: toUpdate?.identificationNumber ?? "",
        slug: toUpdate?.slug ?? "",
        logo: toUpdate?.logo ?? "",
        description: toUpdate?.description ?? "",
        arcGisFeed: toUpdate?.arcGisFeed ?? "",
    });

    const refreshOrThrow = (response: ResponseType<Organization>) => {
        if (response.status === 200 || response.status == 201) {
            router.replace(`/organizations/${response.data?.slug}`);
            router.refresh();
        } else {
            throw { status: response.status };
        }
    };

    const onSubmit = async (e: SyntheticEvent) => {
        e.preventDefault();
        setStatus(null);
        try {
            let response: ResponseType<Organization>;
            if (toUpdate) {
                response = await updateOrganization(toUpdate?.id as number, fields);
            } else {
                response = await addOrganization(fields);
            }
            refreshOrThrow(response);
            setStatus(undefined);
        } catch (error) {
            setStatus(getErrorKey(error.status));
        }
    };

    const getErrorKey = (reason: number | string) => {
        switch (reason) {
            case 400:
                return `${text.errors.validationFailed}`;
            case 409:
                return `${text.errors.duplicateUrlSlug}`;
            default:
                return `${text.errors.unknown}`;
        }
    };

    return (
        <form className={styles["organization-form"]} onSubmit={onSubmit}>
            <TextField
                name="name"
                type="text"
                placeholder={text.organization.form.namePrompt}
                label={text.organization.form.name}
                value={fields.name}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        name: e.target.value,
                    }))
                }
            />
            <TextField
                name="identificationNumber"
                type="number"
                placeholder={text.organization.form.identificationNumberPrompt}
                label={text.organization.form.identificationNumber}
                value={fields.identificationNumber}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        identificationNumber: e.target.value,
                    }))
                }
            />
            <TextField
                name="slug"
                type="text"
                placeholder={text.organization.form.slugPrompt}
                label={text.organization.form.slug}
                value={fields.slug}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        slug: e.target.value,
                    }))
                }
            />
            <TextField
                name="logo"
                type="url"
                placeholder={text.organization.form.logoPrompt}
                label={text.organization.form.logo}
                value={fields.logo}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        logo: e.target.value,
                    }))
                }
            />
            <TextArea
                name="description"
                type="text"
                placeholder={text.organization.form.descriptionPrompt}
                label={text.organization.form.description}
                value={fields.description}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        description: e.target.value,
                    }))
                }
            />
            <TextField
                name="arcGisFeed"
                type="url"
                placeholder={text.organization.form.arcGisFeedPrompt}
                label={text.organization.form.arcGisFeed}
                value={fields.arcGisFeed}
                onChange={(e) =>
                    setFields((prevState) => ({
                        ...prevState,
                        arcGisFeed: e.target.value,
                    }))
                }
            />
            <div className={`${styles["input-button"]}`}>
                <Button color="secondary" type="submit" label={toUpdate ? text.change : text.create} disabled={status === null} />
                <div className={`${styles["error"]}`}>{status}</div>
            </div>
        </form>
    );
};

export default OrganizationForm;
