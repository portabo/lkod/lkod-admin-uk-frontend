import Image from "next/image"; // Importing Next.js Image component
import React, { FC, memo } from "react";

import OrganizationIcon from "@/components/icons/OrganizationIcon";

type OrganizationLogoProps = {
    url: string | null;
    label: string;
    size: "small" | "large";
    shape: "circle" | "square";
};

const OrganizationLogo: FC<OrganizationLogoProps> = ({ url, label, size, shape }) => {
    const logoSize = size === "small" ? 55 : 96;
    const borderRadius = shape === "circle" ? "50%" : "0";

    const style: React.CSSProperties = {
        borderRadius,
        border: "1px solid #CCCCCC",
        objectFit: "contain",
        display: "block",
    };

    return (
        <>
            {url ? (
                <div style={{ width: logoSize, height: logoSize, overflow: "hidden", borderRadius }}>
                    <Image src={url} alt={label} width={logoSize} height={logoSize} style={{ ...style }} layout="responsive" />
                </div>
            ) : (
                <OrganizationIcon color="gray-light" width={logoSize} height={logoSize} style={{ ...style }} />
            )}
        </>
    );
};

export default memo(OrganizationLogo);
