import { User } from "@/services/users-service";

const normalize = (name: string) =>
    name
        .normalize("NFD")
        .replace(/\p{Diacritic}/gu, "")
        .toLowerCase();

const matchesQuery = (name: string, query: string) => normalize(name).includes(normalize(query));

export const userMatchesQuery = (u: User, query: string) => matchesQuery(u.name, query) || matchesQuery(u.email, query);

export default matchesQuery;
