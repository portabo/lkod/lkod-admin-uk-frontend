import React, { FC, KeyboardEvent, ReactNode } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { CopyIcon } from "@/components/icons/CopyIcon";
import { Popup } from "@/components/Popup";
import TextField from "@/components/TextField";
import text from "@/textContent/cs.json";

import styles from "./filePathToClipboardDialog.module.scss";

export interface IFilePathToClipboardDialog {
    isOpen: boolean;
    closePopup: () => void;
    filePath: string | null;
    id?: string | null;
    children: ReactNode;
}

export const FilePathToClipboardDialog: FC<IFilePathToClipboardDialog> = ({ isOpen, closePopup, filePath, children }) => {
    const copyToCBclose = () => {
        navigator.clipboard.writeText(filePath ?? "");
        closePopup();
    };

    const onCopyHandler = (e: KeyboardEvent) => {
        e.preventDefault();
        const charCode = String.fromCharCode(e.which).toLowerCase();

        if ((e.ctrlKey || e.metaKey) && charCode === "c") {
            copyToCBclose();
        }
    };

    const popupContent = (
        <div className={styles.content}>
            <Heading tag="h6">URL souboru</Heading>
            <div className={styles.body}>
                <TextField
                    label={text.copyFilePath}
                    hideLabel
                    value={filePath ?? ""}
                    readOnly
                    onClick={(e) => {
                        e.stopPropagation();
                        e.nativeEvent.stopImmediatePropagation();
                    }}
                    onKeyDown={onCopyHandler}
                    onCopy={() => copyToCBclose()}
                />
                <Button
                    label={text.copyToClipboard}
                    hideLabel
                    color={`primary`}
                    onClick={() => {
                        copyToCBclose();
                    }}
                >
                    <CopyIcon color={`tertiary`} />
                </Button>
            </div>
        </div>
    );

    return (
        <Popup visible={isOpen} hide={closePopup} content={popupContent}>
            <>{children}</>
        </Popup>
    );
};
