import React from "react";

import SearchableUserDialog from "@/modules/users/SearchableUserDialog";
import { Organization } from "@/services/organizations-service";
import { User } from "@/services/users-service";

interface AddMemberDialogProps {
    organization: Organization;
    isOpen: boolean;
    closeDialog: () => void;
    nonMembers: User[];
    onAdd: (user: User) => void;
}

const AddMemberDialog = (props: AddMemberDialogProps) => {
    return (
        <SearchableUserDialog
            label={"Přidat člena organizace"}
            headerText={props.organization.name}
            isOpen={props.isOpen}
            closeDialog={props.closeDialog}
            users={props.nonMembers}
            onSelected={props.onAdd}
        />
    );
};
export default AddMemberDialog;
