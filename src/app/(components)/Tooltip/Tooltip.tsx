import React, { DetailedHTMLProps, HTMLAttributes, useState } from "react";

import styles from "./Tooltip.module.scss";

interface ITooltip extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    text: string | null;
}

export const Tooltip = ({ children, text, ...restProps }: ITooltip) => {
    const [show, setShow] = useState(false);
    return (
        <div
            className={styles["tooltip_wrapper"]}
            onMouseEnter={() => (text === null ? setShow(false) : setShow(true))}
            onMouseLeave={() => setShow(false)}
        >
            <div className={styles["tooltip"]} style={show ? { visibility: "visible" } : {}}>
                {text}
            </div>
            <div {...restProps}>{children}</div>
        </div>
    );
};
