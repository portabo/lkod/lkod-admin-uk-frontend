/* eslint-disable react/display-name */
import React, { ButtonHTMLAttributes, DetailedHTMLProps, ForwardedRef, forwardRef, HTMLAttributes } from "react";

import styles from "./Card.module.scss";

type Tag = "button" | "div";

type CommonProps = {
    tag?: Tag;
    horizontal?: boolean;
    className?: string;
    label?: string;
};

type CardProps = CommonProps & DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> & { href?: undefined };

type CardButtonProps = CommonProps &
    DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & { href?: undefined };

type PolymorphicProps = CardProps | CardButtonProps;

const isButton = (props: PolymorphicProps): props is CardButtonProps => {
    return props.href === undefined && props.tag === "button";
};

export const Card = forwardRef<HTMLDivElement | HTMLButtonElement, PolymorphicProps>((props: PolymorphicProps, ref) => {
    const classes = `${styles["card"]} ${props.horizontal === true ? styles["card_horizontal"] : ""} ${
        props.tag === "button" ? styles["card-active"] : ""
    } ${props.className}`;
    return !isButton(props) ? (
        <div ref={ref as ForwardedRef<HTMLDivElement>} className={classes} {...props} />
    ) : (
        <button
            {...props}
            ref={ref as ForwardedRef<HTMLButtonElement>}
            className={classes}
            aria-label={props.label ?? ""}
            type="button"
        />
    );
});
