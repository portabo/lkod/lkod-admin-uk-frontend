"use client";
/* eslint-disable react/display-name */
/* eslint-disable max-len */

import Link from "next/link";
import { useRouter } from "next/navigation";
import React, { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";

import styles from "./ColorLink.module.scss";

interface ColorLinkProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    linkText: string;
    linkUrl?: string;
    onClick?: () => void;
    blank?: boolean;
    center?: boolean;
    start?: boolean;
    end?: boolean;
    back?: boolean;
    direction: "up" | "down" | "left" | "right";
}

const ColorLink = forwardRef<HTMLDivElement, ColorLinkProps>(
    ({ linkText, linkUrl, onClick, start, center, end, direction, back, ...restProps }: ColorLinkProps, ref) => {
        const router = useRouter();

        const handleClick = () => {
            if (back) {
                router.back();
            } else if (onClick) {
                onClick();
            }
        };

        return (
            <div
                className={`${styles.container} ${center ? styles["container_position_center"] : ""} ${
                    start ? styles["container_position_start"] : ""
                } ${end ? styles["container_position_end"] : ""}`}
                {...restProps}
                ref={ref}
            >
                <button className={styles["color-link"]} onClick={back ? handleClick : onClick} type="button">
                    <ChevronIcon color={`secondary`} direction={direction} />
                    {linkText}
                </button>
                {!back && !onClick && linkUrl && (
                    <div className={styles["color-link"]}>
                        <Link href={linkUrl}>
                            <ChevronIcon color={`secondary`} direction={direction} />
                            {linkText}
                        </Link>
                    </div>
                )}
            </div>
        );
    }
);

export default ColorLink;
