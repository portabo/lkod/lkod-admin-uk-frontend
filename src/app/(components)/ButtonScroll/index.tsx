import React, { ButtonHTMLAttributes, DetailedHTMLProps, FC } from "react";

import { ChevronIcon } from "@/components/icons/ChevronIcon";

import styles from "./ButtonScroll.module.scss";

type Props = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
    direction: "left" | "right";
    ariaLabel: string;
    color: "primary" | "primary-light" | "secondary" | "tertiary" | "outline-secondary" | "outline-gray";
};

export const ButtonScroll: FC<Props> = ({ onClick, color, direction, ariaLabel, disabled }) => {
    return (
        <button
            type="button"
            className={`${styles["button-scroll"]} ${styles[`button-scroll-color_${color}`]}`}
            onClick={onClick}
            aria-label={ariaLabel}
        >
            <ChevronIcon direction={direction} color={`${disabled ? "gray-light" : "tertiary"}`} />
        </button>
    );
};
