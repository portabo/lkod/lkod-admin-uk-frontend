import React, { useCallback } from "react";

import text from "@/textContent/cs.json";

export const useHighlightedName = () => {
    const getHighlightedName = useCallback((name?: string | null) => {
        return name ? <b>{name}</b> : <i>{text.unnamed}</i>;
    }, []);

    return {
        getHighlightedName,
    };
};
