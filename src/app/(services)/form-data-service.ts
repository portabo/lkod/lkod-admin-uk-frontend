import { ApiClient, ResponseType } from "./api-client";
export interface ReceiveFormDataRequest {
    formData: string;
    userData: string;
}

export class FormDataService {
    async receiveFormData(body: ReceiveFormDataRequest): Promise<ResponseType<void>> {
        const formParams = new FormData();

        formParams.append("formData", body.formData);
        formParams.append("userData", body.userData);

        return await ApiClient.post<void>("form-data", formParams, { "Content-Type": "application/x-www-form-urlencoded" });
    }
}
