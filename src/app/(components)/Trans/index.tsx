import React from "react";

interface ITransProps {
    i18nKey: string;
    values?: Record<string, string>;
    components?: React.ReactNode[];
}

const Trans: React.FC<ITransProps> = ({ i18nKey, values = {}, components = [] }) => {
    const translate = (key: string, values: Record<string, string> = {}) => {
        const translations: Record<string, string> = {
            "errors.fileNotFound": "Soubor <0>{{filename}}</0> nenalezen",
            "errors.unknownDeleteFileError": "Došlo k neznámé chybě při mazání <0>{{filename}}</0>",
            fileRewritten: "Soubor <0>{{filename}}</0> byl přepsán",
            "erors.uploadFile": "Nahrávání souboru se nezdařilo",
        };

        let translation = translations[key] || key;

        // Replace placeholders in the translation with values
        Object.keys(values).forEach((placeholder) => {
            translation = translation.replace(new RegExp(`{{${placeholder}}}`, "g"), values[placeholder]);
        });

        return translation;
    };

    const translatedText = translate(i18nKey, values);

    // Replace placeholders in the translation with provided components
    const elements = translatedText.split(/(@@@\d+@@@)/).map((part, index) => (index % 2 === 0 ? part : components[index / 2]));

    return <>{elements}</>;
};

export default Trans;
