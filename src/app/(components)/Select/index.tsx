import React, { DetailedHTMLProps, SelectHTMLAttributes } from "react";

import styles from "./Select.module.scss";

interface SelectProps extends DetailedHTMLProps<SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement> {
    label: string;
}

export const Select = ({ label, className, children, ...restProps }: SelectProps) => {
    return (
        <>
            <label className={styles["label"]}>
                {label}

                <span className={styles["input-wrapper"]}>
                    <select className={`${className} ${styles["input-area"]}`} aria-label={label} {...restProps}>
                        {children}
                    </select>
                </span>
            </label>
        </>
    );
};
