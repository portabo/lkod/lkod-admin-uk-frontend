"use client";
import React, { useState } from "react";

import Button from "@/components/Button";
import { FolderPlusIcon } from "@/components/icons/FolderPlusIcon";
import AddOrganizationDialog from "@/organizations/(components)/AddOrganizationDialog";
import styles from "@/organizations/Organizations.module.scss";

const AddOrganizationButton = () => {
    const [addNewOpen, setAddNewOpen] = useState(false);

    return (
        <>
            <Button
                color={`secondary`}
                label={`Přidat organizaci`}
                className={styles["add-organization"]}
                onClick={() => {
                    setAddNewOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                }}
            >
                <FolderPlusIcon color="tertiary" />
            </Button>
            <AddOrganizationDialog isOpen={addNewOpen} closeDialog={() => setAddNewOpen(false)} />
        </>
    );
};

export default AddOrganizationButton;
