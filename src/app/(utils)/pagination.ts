export const paginate = <T>(items: T[], page: string, perpage: string, perPage: number): T[] => {
    const currentPage = page ? Number(page) : 1;
    const currentPerPage = perpage ? Number(perpage) : perPage;
    const itemOffsetIndex = (currentPage - currentPerPage / perPage) * perPage;
    return items.slice(itemOffsetIndex, itemOffsetIndex + currentPerPage);
};
