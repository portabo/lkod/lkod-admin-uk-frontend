import { useCallback } from "react";

import { getLogger } from "../(logging)/logger";
import { dataServiceClient } from "../(services)/client/datasets-service-client";
import { getErrorMessage } from "../(utils)/getErrorMessage";

const logger = getLogger("useDatasetListDownload");

export const useDatasetListDownload = () => {
    const getDatasetList = useCallback(async () => {
        try {
            const response = await dataServiceClient.getDatasetsListFile();
            if (!response) {
                throw new Error("Data retrieval failed.");
            }

            const downloadUrl = URL.createObjectURL(response);
            const anchorElement = document.createElement("a");
            anchorElement.href = downloadUrl;
            anchorElement.download = "datasetsList.csv";
            document.body.appendChild(anchorElement);
            anchorElement.click();
            document.body.removeChild(anchorElement);
            URL.revokeObjectURL(downloadUrl);
        } catch (error) {
            logger.error({
                component: "useDatasetListDownload",
                message: getErrorMessage(error),
            });
        }
    }, []);

    return { getDatasetList };
};
