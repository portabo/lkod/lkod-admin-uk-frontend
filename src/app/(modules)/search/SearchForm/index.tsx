"use client";
import { useRouter, useSearchParams } from "next/navigation";
import React, { forwardRef, KeyboardEvent, useEffect, useState } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { SearchInput } from "@/root/app/(components)/SearchInput";
import text from "@/root/app/(textContent)/cs.json";

import styles from "./SearchForm.module.scss";

type SearchFormProps = {
    label?: string;
    fullWidth?: boolean;
    placeholder?: string;
    asYouType?: (query: string) => void;
};

export const SearchForm = forwardRef<HTMLDivElement, SearchFormProps>((SearchFormProps, ref) => {
    const { label, fullWidth, placeholder, asYouType } = SearchFormProps;
    const router = useRouter();
    const searchParams = useSearchParams();
    const search = searchParams.get("search");

    const [inputValue, setInputValue] = useState("");
    const updateInputValue = (value: string) => {
        setInputValue(value);
    };

    useEffect(() => {
        if (asYouType) {
            asYouType(inputValue);
        }
    }, [asYouType, inputValue]);

    const searchClicked = () => {
        if (asYouType) {
            asYouType(inputValue);
        } else {
            router.push(`?search=${encodeURIComponent(inputValue)}`, { scroll: false });
        }
    };

    const onKeyHandler = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            searchClicked();
        }
    };

    const resetInputField = () => {
        updateInputValue("");
        if (asYouType) {
            asYouType("");
        } else {
            router.push(`?`);
        }
    };

    useEffect(() => {
        if (!search) {
            setInputValue("");
        } else {
            setInputValue(search as string);
        }
    }, [search]);

    return (
        <div className={`${styles["search-form"]} ${fullWidth ? styles.full : ""}`} ref={ref}>
            <div className={styles.row}>
                {label ? (
                    <Heading tag={`h2`} type={`h4`}>
                        {label}
                    </Heading>
                ) : (
                    ""
                )}
            </div>
            <div className={styles.row}>
                <SearchInput
                    name="dataSearch"
                    aria-labelledby="searchButton"
                    placeholder={placeholder ?? text.searchInputText}
                    onKeyDown={onKeyHandler}
                    resetInputField={resetInputField}
                    updateInputValue={updateInputValue}
                    value={inputValue}
                />
                {!!asYouType || <Button id="searchButton" color="secondary" label="Vyhledat" onClick={searchClicked} />}
            </div>
        </div>
    );
});
