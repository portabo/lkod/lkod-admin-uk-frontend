import Link from "next/link";
import React, { FC, useCallback, useEffect, useMemo, useRef, useState } from "react";

import Button from "@/components/Button";
import { Card } from "@/components/Card";
import { DeleteDatasetFileButton } from "@/components/DeleteDatasetFileButton";
import { FilePathToClipboardDialog } from "@/components/FilePathToClipboardDialog";
import { Heading } from "@/components/Heading";
import HorizontalLine from "@/components/HorizontalLine";
import { FileDownloadIcon } from "@/components/icons/FileDownloadIcon";
import { FileIcon } from "@/components/icons/FileIcon";
import { LinkIcon } from "@/components/icons/LinkIcon";
import { PlusIcon } from "@/components/icons/PlusIcon";
import { Modal } from "@/components/Modal";
import { Spinner } from "@/components/Spinner";
import { useFilesState } from "@/hooks/useFilesState";
import { NotificationType, useNotifications } from "@/hooks/useNotifications";
import { useUploadFileState } from "@/hooks/useUploadFileState";
import text from "@/textContent/cs.json";
import { ValidatedDataset } from "@/types/ValidatedDatasetInterface";

import Trans from "../Trans";
import styles from "./filesDialog.module.scss";

export const FilesDialog: FC<{
    isOpen: boolean;
    closeDialog: () => void;
    dataset: ValidatedDataset | null;
}> = ({ isOpen, closeDialog, dataset }) => {
    const datasetId = dataset?.id || null;
    const { files, isLoading: areFilesLoading, hasFailed: haveFilesFailed, refreshFiles } = useFilesState(datasetId);

    const { addNotification } = useNotifications();

    const [filePath, setFilePath] = useState<string | null>(null);

    const hiddenFileInput = useRef<HTMLInputElement>(null);

    const onFileUpload = useCallback(
        (filename: string) => {
            const fileWithSameName = (files as string[]).find((fileUrl) => {
                const fileUrlParts = fileUrl.split("/");
                return fileUrlParts[fileUrlParts.length - 1] === filename;
            });

            if (fileWithSameName) {
                addNotification(
                    <Trans i18nKey="fileRewritten" values={{ filename }} components={[<b key={1} />]} />,
                    NotificationType.SUCCESS
                );
            }

            refreshFiles();
        },
        [files, addNotification, refreshFiles]
    );

    const { isLoading: isUploadLoading, uploadFile, hasFailed } = useUploadFileState(datasetId, onFileUpload);

    useEffect(() => {
        if (hasFailed) {
            addNotification(<Trans i18nKey="errors.uploadFile" />, NotificationType.ERROR);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hasFailed]);

    const isLoading = isUploadLoading || areFilesLoading;

    const transformedFiles = useMemo(
        () =>
            (files as string[]).map((file) => {
                const split = file.split("/");
                const filename = split[split.length - 1];

                return { name: filename, url: file };
            }),
        [files]
    );

    const handleClick = () => {
        if (hiddenFileInput.current) {
            hiddenFileInput.current.click();
        }
    };

    const onFileInputChange = (inputFiles: FileList | null) => {
        if (!inputFiles?.length) {
            return;
        }

        const file = inputFiles[0];
        uploadFile(file);
    };

    const handlePath = (path: string) => {
        if (filePath) {
            setFilePath(null);
        } else {
            setFilePath(path);
        }
    };

    const handleFullClose = () => {
        closeDialog();
        setFilePath(null);
    };

    return (
        <Modal
            show={isOpen}
            onClose={handleFullClose}
            onDrop={(e) => {
                e.preventDefault();
                onFileInputChange(e.dataTransfer.files);
            }}
            onDragOver={(e) => e.preventDefault()}
            label={text.files}
            headerText={dataset?.name ?? ""}
        >
            {!!dataset && !!datasetId && (
                <>
                    <HorizontalLine />
                    <div className={styles["files-container"]}>
                        {isLoading && <Spinner />}
                        {haveFilesFailed && (
                            <Heading tag={`h6`} color="secondary">
                                {text.errors.filesDownloadFailed}
                            </Heading>
                        )}
                        {!isLoading && !haveFilesFailed && (
                            <div className={styles["files-area"]}>
                                {transformedFiles.map((file) => (
                                    <Card className={styles["file-card"]} key={file.url}>
                                        <div className={styles["file-card__row"]}>
                                            <FileIcon color={`gray-light`} style={{ minWidth: "1.5rem", height: "2rem" }} />
                                            <div className={styles["file-card__col"]}>
                                                <Heading tag={`h5`} type={`h6`} className={styles["file-card__label"]}>
                                                    {file.name}
                                                </Heading>
                                                <DeleteDatasetFileButton
                                                    datasetId={datasetId}
                                                    filename={file.name}
                                                    onError={addNotification}
                                                />
                                            </div>
                                        </div>
                                        <div className={styles["file-card__btns"]}>
                                            <FilePathToClipboardDialog
                                                id={file.url}
                                                isOpen={!!filePath}
                                                closePopup={() => {
                                                    setFilePath(null);
                                                }}
                                                filePath={filePath}
                                            >
                                                <Button
                                                    color={`outline-gray`}
                                                    onClick={() => handlePath(file.url)}
                                                    label={text.openFilePathWindow}
                                                    hideLabel
                                                >
                                                    <LinkIcon color={`gray`} style={{ minWidth: "1rem" }} />
                                                </Button>
                                            </FilePathToClipboardDialog>
                                            <Link href={file.url} target={`_blank`} download={file.name}>
                                                <Button label={text.download} color={`primary`} className={styles.btn}>
                                                    <FileDownloadIcon color={`tertiary`} />
                                                </Button>
                                            </Link>
                                        </div>
                                    </Card>
                                ))}
                                {(files as string[]).length === 0 && (
                                    <Heading tag={`h4`} type={`h5`}>
                                        {text.noFiles}
                                    </Heading>
                                )}
                            </div>
                        )}
                    </div>
                    <div className={styles.actions}>
                        <Button color="outline-gray" onClick={handleFullClose} label={text.close} />
                        <Button
                            color="secondary"
                            disabled={isLoading || haveFilesFailed}
                            onClick={handleClick}
                            label={text.addFile}
                        >
                            <input
                                type="file"
                                aria-label={text.addFile}
                                hidden={true}
                                ref={hiddenFileInput}
                                onChange={(e) => onFileInputChange(e.currentTarget.files)}
                            />
                            <PlusIcon color={`tertiary`} height={`1.3rem`} />
                        </Button>
                    </div>
                </>
            )}
        </Modal>
    );
};
