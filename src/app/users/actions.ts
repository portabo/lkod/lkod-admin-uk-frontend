"use server";

import { ResponseType } from "@/services/api-client";
import { ApiClient } from "@/services/initApi";
import { User, UserFields } from "@/services/users-service";

export async function addUser(usr: UserFields): Promise<ResponseType<User>> {
    const { usersApi } = ApiClient;
    return await usersApi.addUser(usr);
}

export async function updateUser(id: number, usr: UserFields): Promise<ResponseType<User>> {
    const { usersApi } = ApiClient;
    return await usersApi.updateUser(id, usr);
}

export async function deleteUser(userId: number): Promise<ResponseType<void>> {
    const { usersApi } = ApiClient;
    return await usersApi.deleteUser(userId);
}
