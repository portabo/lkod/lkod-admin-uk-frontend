import pino, { Logger } from "pino";
import { config } from "src/config";

export const getLogger = (name: string): Logger => {
    return pino({ name, level: config.NEXT_PUBLIC_LOG_LEVEL || "info" });
};
