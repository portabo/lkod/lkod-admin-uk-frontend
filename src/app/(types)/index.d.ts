export {};

declare global {
    interface Window {
        ENV: {
            EXTERNAL_FORM_URL: string;
            BACKEND_URL: string;
            RETURN_URL: string;
        };
    }
}
