import React, { FC, useContext } from "react";

import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
import { ReaderIcon } from "@/components/icons/ReaderIcon";
import { DatasetsContext } from "@/state/datasetsState";
import text from "@/textContent/cs.json";
import { Dataset } from "@/types/Dataset";

export const DeleteDatasetDialog: FC<{
    isOpen: boolean;
    datasetToDelete: Dataset | null;
    closeDialog: () => void;
}> = ({ isOpen, datasetToDelete, closeDialog }) => {
    const { deleteDataset } = useContext(DatasetsContext);

    return (
        <>
            {datasetToDelete && (
                <DeleteDialog
                    isOpen={isOpen}
                    name={datasetToDelete.name}
                    onConfirm={() => {
                        deleteDataset(datasetToDelete.id);
                    }}
                    closeDialog={closeDialog}
                    titleIcon={<ReaderIcon color={`gray-light`} style={{ minWidth: "1.5rem", height: "1.5rem" }} />}
                    titleText={text.dataset.delete.prompt}
                    deleteButtonText={text.dataset.delete.action}
                />
            )}
        </>
    );
};
