"use client";
import { usePathname, useRouter } from "next/navigation";
import React, { use, useCallback, useContext, useEffect, useRef, useState } from "react";
import { config } from "src/config";

import Button from "@/components/Button";
import { EditIcon } from "@/components/icons/EditIcon";
import { EyeIcon } from "@/components/icons/Eye";
import { EyeSlashIcon } from "@/components/icons/Eye-slash";
import { FileIcon } from "@/components/icons/FileIcon";
import { TrashIcon } from "@/components/icons/TrashIcon";
import UserIcon from "@/components/icons/UserIcon";
import { WarningIcon } from "@/components/icons/WarningIcon";
import { XCircleIcon } from "@/components/icons/XCircleIcon";
import { ResultLine } from "@/components/ResultLine";
import { Text } from "@/components/Text";
import { Tooltip } from "@/components/Tooltip/Tooltip";
import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import DatasetCard from "@/modules/datasets/DatasetCard";
import SearchableUserDialog from "@/modules/users/SearchableUserDialog";
import { User } from "@/services/users-service";
import text from "@/textContent/cs.json";
import { IDatasetValidationData, ValidatedDataset } from "@/types/ValidatedDatasetInterface";

import { DeleteDatasetDialog } from "../../(components)/DeleteDatasetDialog";
import { FilesDialog } from "../../(components)/FilesDialog";
import { Paginator } from "../../(components)/Paginator/Paginator";
import { ValidationMessageDialog } from "../../(components)/ValidationMessageDialog/validationMessageDialog";
import { useThemesPromise } from "../../(context)/DataProvider";
import { useFilesStatus } from "../../(hooks)/useFilesStatus";
import { useUrlFilter } from "../../(hooks)/useUrlFilter";
import { DatasetsContext } from "../../(state)/datasetsState";
import { DatasetStatus } from "../../(types)/Dataset";
import { capitalizeFirstLetter } from "../../(utils)/helpers";
import { objectToQueryString } from "../../(utils)/objectToQueryString";
import styles from "./datasetListCard.module.scss";

export const DATASET_PER_PAGE_COUNT = 6;

interface IDatasetListCard {
    count: number;
    datasets: ValidatedDataset[];
    users: User[];
    searchParams: {
        page: string;
        search: string;
        perpage: string;
    };
}

const DatasetsListCard = ({ count, datasets, users, searchParams }: IDatasetListCard) => {
    const router = useRouter();
    const pathname = usePathname();

    const { page, perpage, search } = searchParams;

    const [scrollY] = useState<number | undefined>(undefined);
    const size: ISize = useWindowSize();
    const [isLoading, setIsLoading] = useState(false);
    const topics = use(useThemesPromise());
    const { urlFilter, searchString } = useUrlFilter();

    const currentPage = page ? Number(page) : 1;
    const currentPerPage = perpage ? Number(perpage) : DATASET_PER_PAGE_COUNT;
    const pageCount = Math.ceil(count / DATASET_PER_PAGE_COUNT) || 1;
    const isCurrentPageLast = currentPage === pageCount;

    const isFromArcGis = datasets.length > 0 && datasets[0].isReadOnly;
    const showMoreRef = useRef<HTMLButtonElement | null>(null);

    const { datasetsError, isEditLoading, editDataset, updateDataset } = useContext(DatasetsContext);

    const [datasetToDelete, setDatasetToDelete] = useState<ValidatedDataset | null>(null);
    const [datasetToTransfer, setDatasetToTransfer] = useState<ValidatedDataset | null>(null);
    const [detailDataset, setDetailDataset] = useState<ValidatedDataset | null>(null);
    const [isDeleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
    const { areFilesAvailable } = useFilesStatus();
    const [validationResultData, setValidationResultData] = useState<IDatasetValidationData | null>(null);
    const refreshData = () => {
        setIsLoading(true);
    };
    const urlFilterString = objectToQueryString(urlFilter);

    const urlSearchString = searchString ? `&search=${searchString}` : "";

    const perPageHandler = () => {
        const lastItem = currentPage * DATASET_PER_PAGE_COUNT;
        router.push(
            `${pathname}?${urlFilterString}${urlSearchString}&page=${
                lastItem < count && lastItem + DATASET_PER_PAGE_COUNT < count ? currentPage + 1 : pageCount
            }&perpage=${!isCurrentPageLast ? currentPerPage + DATASET_PER_PAGE_COUNT : currentPerPage}`,
            { scroll: false }
        );
    };
    const showDeleteConfirmation = useCallback((dataset: ValidatedDataset) => {
        setDatasetToDelete(dataset);
        setDeleteConfirmOpen(true);
    }, []);

    const closeDialog = useCallback(() => {
        setDeleteConfirmOpen(false);
    }, []);

    useEffect(() => {
        refreshData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [page, perpage, search]);

    useEffect(() => {
        if (perpage && +perpage > DATASET_PER_PAGE_COUNT) {
            scrollY && window.scrollTo({ top: scrollY });
        }
        setIsLoading(false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [datasets, perpage]);
    return (
        <div className={styles["datasets-container"]} aria-hidden={!!detailDataset || isDeleteConfirmOpen}>
            {(!isLoading || !datasetsError) && (
                <ResultLine
                    showDownloadDatasets
                    result={count}
                    foundPlurals={text.dataset.search.foundPlurals}
                    plurals={text.dataset.search.plurals}
                />
            )}
            <div className={styles["datasets-list"]}>
                {(isLoading || !!datasetsError) && <Text>{datasetsError}</Text>}
                {datasets.map((dataset: ValidatedDataset) => {
                    const owner = users.find((u) => dataset.userId === u.id);
                    const theme = topics.find((topic) => topic.iri === (dataset.data?.téma as string[] | undefined)?.[0]);
                    const themeLabel = theme ? capitalizeFirstLetter(theme.label) : "n/a";
                    return (
                        <div key={dataset.id} className={styles["shadow-wrapper"]}>
                            <DatasetCard
                                id={dataset.id}
                                isReadOnly={dataset.isReadOnly}
                                organizationId={dataset.organizationId}
                                userId={dataset.userId}
                                status={dataset.status}
                                createdAt={dataset.createdAt}
                                updatedAt={dataset.updatedAt}
                                organization={dataset.organization}
                                data={dataset.data}
                                name={dataset.name}
                                theme={themeLabel}
                                validationResult={dataset.validationResult}
                                user={owner}
                            />
                            <div className={styles["button-holder"]}>
                                {dataset.validationResult && dataset.validationResult.status !== "valid" && (
                                    <Tooltip text={`text.datasetValidation.${dataset.validationResult.status}`}>
                                        <Button
                                            label={`${
                                                dataset.validationResult.status === "hasWarnings" ? `UPOZORNĚNÍ` : `CHYBY`
                                            } (${dataset.validationResult.messages?.length})`}
                                            hideLabelMobile
                                            color={`${
                                                dataset.validationResult.status === "hasWarnings"
                                                    ? "outline_warning"
                                                    : "outline_alert"
                                            }`}
                                            onClick={() => {
                                                setValidationResultData({
                                                    result: dataset.validationResult,
                                                    label: dataset.name,
                                                });
                                            }}
                                        >
                                            {dataset.validationResult.status === "hasWarnings" ? (
                                                <WarningIcon color="warning" />
                                            ) : (
                                                <XCircleIcon color="alert" />
                                            )}
                                        </Button>
                                    </Tooltip>
                                )}
                                {areFilesAvailable && (
                                    <Tooltip text={config.FILES_BUTTON_DISABLED !== "false" ? text.filesButtonDisabled : null}>
                                        <Button
                                            label={text.files}
                                            hideLabelMobile
                                            color={"outline-gray"}
                                            type="button"
                                            disabled={isEditLoading || config.FILES_BUTTON_DISABLED !== "false" || isFromArcGis}
                                            onClick={() => {
                                                setDetailDataset(dataset);
                                            }}
                                        >
                                            <FileIcon color="black" style={{ minWidth: "0.9rem" }} />
                                        </Button>
                                    </Tooltip>
                                )}
                                <Button
                                    label={text.edit}
                                    hideLabelMobile
                                    color={"outline-gray"}
                                    type="button"
                                    disabled={isEditLoading || isFromArcGis}
                                    onClick={() => {
                                        editDataset(dataset);
                                    }}
                                >
                                    <EditIcon color="black" style={{ minWidth: "1.2rem" }} />
                                </Button>
                                <Button
                                    label={dataset.status === DatasetStatus.Published ? text.unpublish : text.publish}
                                    hideLabelMobile
                                    color={"outline-gray"}
                                    type="button"
                                    disabled={dataset.status === DatasetStatus.Created || isEditLoading || isFromArcGis}
                                    onClick={() => {
                                        updateDataset(dataset, dataset.status !== DatasetStatus.Published);
                                    }}
                                >
                                    {dataset.status === DatasetStatus.Published ? (
                                        <EyeSlashIcon color="black" style={{ minWidth: "1.2rem" }} />
                                    ) : (
                                        <EyeIcon color="black" style={{ minWidth: "1.1rem" }} />
                                    )}
                                </Button>
                                {owner && (
                                    <Button
                                        type="button"
                                        color={"outline-gray"}
                                        label={"Vlastnictví"}
                                        onClick={() => setDatasetToTransfer(dataset)}
                                    >
                                        <UserIcon color="black" style={{ minWidth: "0.9rem" }} />
                                    </Button>
                                )}
                                <Button
                                    type="button"
                                    color={"outline-secondary"}
                                    onClick={() => {
                                        showDeleteConfirmation(dataset);
                                    }}
                                    label={text.delete}
                                    hideLabel
                                    disabled={isFromArcGis}
                                >
                                    <TrashIcon color="secondary" style={{ minWidth: "0.9rem" }} />
                                </Button>
                            </div>
                        </div>
                    );
                })}
            </div>
            <div className={styles["datasets-navigation"]}>
                {count > DATASET_PER_PAGE_COUNT && !isCurrentPageLast && (
                    <Button color={`secondary`} label={`Zobrazit další`} onClick={perPageHandler} ref={showMoreRef} />
                )}
                <Paginator
                    totalCount={count}
                    defaultItemCount={DATASET_PER_PAGE_COUNT}
                    currentPage={currentPage}
                    nItemsWithin={size.width && size.width > 768 ? (size.width > 1200 ? 3 : 2) : 1}
                />
            </div>
            <FilesDialog closeDialog={() => setDetailDataset(null)} dataset={detailDataset} isOpen={!!detailDataset} />
            <DeleteDatasetDialog closeDialog={closeDialog} datasetToDelete={datasetToDelete} isOpen={isDeleteConfirmOpen} />
            <ValidationMessageDialog
                isOpen={!!validationResultData}
                validationResultData={validationResultData}
                closeDialog={() => setValidationResultData(null)}
            />
            <SearchableUserDialog
                isOpen={!!datasetToTransfer}
                closeDialog={() => setDatasetToTransfer(null)}
                label={"Převést vlastnictví datové sady"}
                headerText={datasetToTransfer?.name ?? ""}
                users={users}
                onSelected={(u) => {
                    const target = datasetToTransfer as ValidatedDataset;
                    updateDataset(target, target.status === DatasetStatus.Published, u.id);
                    setDatasetToTransfer(null);
                }}
            />
        </div>
    );
};

export default DatasetsListCard;
