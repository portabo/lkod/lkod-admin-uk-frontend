"use client";
import { getLogger } from "../(logging)/logger";
import { UploadedFile, UploadFileRequest } from "../(types)/Dataset";
import { getErrorMessage } from "../(utils)/getErrorMessage";

export type ResponseType<T> = {
    count: string | null;
    data: T | null;
    status: number;
    error?: string;
};
const logger = getLogger("UploadFile");

export const UploadFile = async (body: UploadFileRequest): Promise<ResponseType<UploadedFile>> => {
    try {
        const formData = new FormData();
        formData.append("datasetFile", body.datasetFile, body.datasetFile.name);
        formData.append("path", `datasets/${body.datasetId}/files`);
        const response = await fetch(`api/form-data`, {
            method: "POST",
            body: formData,
        });

        if (!response.ok) {
            logger.error({
                component: "UploadFile",
                status: response.status,
                message: `Failed to upload file ${response.statusText}`,
            });
            throw new Error("Failed to upload file");
        }

        const responseData = await response.json();

        return {
            status: response.status,
            count: response.headers.get("X-Total-Count"),
            data: responseData,
        };
    } catch (error) {
        logger.error({
            component: "UploadFile",
            status: error.status,
            message: getErrorMessage(error),
        });
        throw error;
    }
};
