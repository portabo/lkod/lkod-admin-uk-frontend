"use client";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import React, { useEffect } from "react";

import { getLogger } from "@/logging/logger";

import { getErrorMessage } from "../(utils)/getErrorMessage";
import { NotificationType, useNotifications } from "./useNotifications";

const logger = getLogger("useQueryErrorMessage");

interface ErrorObject {
    error_class_name?: string;
    error_info: string;
    error_message: string;
    error_status: number;
}

const parseErrorFromQuery = (query: string | null): { errorStatus: number; errorMessage: string } | null => {
    if (!query || !(query.includes("&e=") || query.includes("?e="))) {
        return null;
    }

    try {
        const errorQueryStartSplit = query.split(/[&?]e=/);

        if (errorQueryStartSplit.length < 2) {
            return null;
        }

        const errorQuery = errorQueryStartSplit[1].split("&")[0];

        const decodedError = decodeURIComponent(errorQuery);
        const errorObject: ErrorObject = JSON.parse(decodedError);

        return {
            errorStatus: errorObject.error_status,
            errorMessage: errorObject.error_message,
        };
    } catch (error) {
        logger.error({
            provider: "client",
            component: "parseErrorFromQuery",
            message: getErrorMessage(error),
            error: error,
        });
        return null;
    }
};

export const useQueryErrorMessage = () => {
    const router = useRouter();
    const { addNotification } = useNotifications();
    const pathname = usePathname();
    const searchParams = useSearchParams();
    const search = searchParams ? searchParams.toString() : "";
    const hash = searchParams ? searchParams.get("hash") : "";

    useEffect(() => {
        const parsedError = parseErrorFromQuery(search);

        if (parsedError) {
            addNotification(
                <>
                    <b>{parsedError.errorStatus}</b>: {parsedError.errorMessage}
                </>,
                NotificationType.ERROR
            );

            router.replace(`${pathname}${hash ? `#${hash}` : ""}`);
        }
        // eslint-disable-next-line
    }, [search]);
};
