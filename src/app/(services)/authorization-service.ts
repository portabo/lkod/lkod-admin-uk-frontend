import { config } from "src/config";

import { getLogger } from "../(logging)/logger";
import { getErrorMessage } from "../(utils)/getErrorMessage";
import { ApiClient, ResponseType } from "./api-client";
import { RequestInit } from "./client/datasets-service-client";

export interface ResetPasswordInput {
    token: string;
    password: string;
}

export interface ChangePasswordInput {
    currentPassword: string;
    newPassword: string;
}

export interface ForgotPasswordInput {
    email: string;
}
export enum UserRole {
    user = "user",
    admin = "admin",
    superadmin = "superadmin",
}
export interface LoginOutput {
    accessToken: string;
    hasDefaultPassword: boolean;
    role: UserRole;
}

export interface LoginInput {
    email?: string;
    password?: string;
}
const logger = getLogger("AuthorizationServices");

export class AuthorizationServices {
    private apiBase: string;

    constructor() {
        this.apiBase = `${config.NEXT_PUBLIC_URL || ""}/api/`;
    }
    private async fetchData<T>({ method, requestUrl, body, headers }: RequestInit): Promise<ResponseType<T>> {
        const startTime = Date.now();
        const fullUrl = `${this.apiBase}datasets-service`;

        const logData: Record<string, unknown> = {
            provider: "server",
            class: "AuthorizationServices",
            method,
            fullUrl,
            body,
            headers,
        };
        try {
            const clientRouteRequestParams: RequestInit = {
                method,
                requestUrl,
                body,
                headers,
            };

            logger.debug({
                ...logData,
                message: "Initiating fetch request",
            });

            const response = await fetch(`${this.apiBase}datasets-service`, {
                method: method,
                body: JSON.stringify(clientRouteRequestParams),
                next: { revalidate: 0 },
                cache: "no-store",
            });

            const endTime = Date.now();
            if (!response.ok) {
                const errorMessage = `Failed to fetch ${fullUrl}: ${response.statusText}`;
                logger.error({
                    ...logData,
                    status: response.status,
                    duration: `${endTime - startTime}ms`,
                    message: `Request failed - response: ${response}`,
                });
                throw new Error(errorMessage);
            }

            const result = await response.json();
            logger.debug({
                ...logData,
                duration: `${endTime - startTime}ms`,
                message: `Request successful - status: ${response.status}`,
            });

            return result;
        } catch (error) {
            const endTime = Date.now();
            logger.error({
                ...logData,
                duration: `${endTime - startTime}ms`,
                status: error.status,
                message: getErrorMessage(error),
            });
            throw new Error(`Error fetching ${fullUrl}: ${error.message}`);
        }
    }

    async changePassword(body: ChangePasswordInput): Promise<ResponseType<void>> {
        if (!body.currentPassword || !body.newPassword) {
            const errorMessage = "Required parameters not defined when calling changePassword.";
            logger.error({ provider: "server", message: "Required parameters not defined when calling changePassword." });
            throw new Error(errorMessage);
        }
        return this.fetchData<void>({ method: "POST", requestUrl: "auth/change-password", body: body });
    }

    async forgotPassword(body: ForgotPasswordInput): Promise<ResponseType<void>> {
        if (!body.email) {
            const errorMessage = "Required parameters not defined when calling forgotPassword.";
            logger.error({ provider: "server", message: errorMessage });
            throw new Error(errorMessage);
        }
        return this.fetchData<void>({ method: "POST", requestUrl: "auth/forgot-password", body });
    }

    async resetPassword(body: ResetPasswordInput): Promise<ResponseType<void>> {
        if (!body.password || !body.token) {
            const errorMessage = "Required parameters not defined when calling resetPassword.";
            logger.error({ provider: "server", message: errorMessage });
            throw new Error(errorMessage);
        }
        return this.fetchData<void>({ method: "POST", requestUrl: "auth/reset-password", body });
    }

    async login(body: LoginInput | undefined): Promise<ResponseType<LoginOutput>> {
        if (!body?.email || !body?.password) {
            const errorMessage = "Required parameters not defined when calling login.";
            logger.error({ provider: "server", message: errorMessage });
            throw new Error(errorMessage);
        }
        return this.fetchData<LoginOutput>({ method: "POST", requestUrl: "auth/login", body });
    }

    async logout(): Promise<ResponseType<void>> {
        logger.debug({ provider: "server", message: "Calling logout API" });
        return ApiClient.post<void>("auth/logout", undefined);
    }
}
const authorizationServices = new AuthorizationServices();
export { authorizationServices };
