import React, { FC } from "react";

import { Paragraph } from "@/components/Paragraph";
import facebook from "@/images/facebook.svg";
import instagram from "@/images/instagram.svg";
import twitter from "@/images/twitter.svg";
import text from "@/textContent/cs.json";

import SocialIcon from "../SocialIcon/SocialIcon";
import styles from "./Socials.module.scss";

const socialIcons = [
    { name: "Facebook", url: facebook, link: "https://www.facebook.com/Prahaeu" },
    { name: "Instagram", url: instagram, link: "https://www.instagram.com/cityofprague" },
    { name: "Twitter", url: twitter, link: "https://twitter.com/PrahaEU" },
];

export const Socials: FC = () => {
    return (
        <div className={styles.socials}>
            <Paragraph>{text.footer.socialsHeading}</Paragraph>
            <div className={styles["social-icons"]}>
                {socialIcons.map((icon, i) => {
                    return <SocialIcon label={icon.name} url={icon.url} link={icon.link} key={i} />;
                })}
            </div>
        </div>
    );
};
