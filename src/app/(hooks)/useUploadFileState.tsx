import { useCallback, useMemo, useReducer } from "react";

import { UploadFile } from "../(services)/upload-file";

interface State {
    isLoading: boolean;
    hasFailed: boolean;
}

type Action = { type: "INITIAL" | "SEND_FILE" | "ERROR" | "FILE_SENT" };

const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case "SEND_FILE":
            return { isLoading: true, hasFailed: false };
        case "ERROR":
            return { isLoading: false, hasFailed: true };
        case "FILE_SENT":
            return { isLoading: false, hasFailed: false };
        case "INITIAL":
            return { ...state };
    }
};

export const useUploadFileState = (datasetId: string | null, onUpload: (filename: string) => void) => {
    const [state, dispatch] = useReducer(reducer, {
        isLoading: false,
        hasFailed: false,
    });

    const uploadFile = useCallback(
        (datasetFile: File) => {
            if (datasetId && !state.isLoading) {
                dispatch({ type: "SEND_FILE" });
                UploadFile({ datasetId, datasetFile })
                    .then(() => {
                        dispatch({ type: "FILE_SENT" });

                        onUpload(datasetFile.name);
                    })
                    .catch(() => dispatch({ type: "ERROR" }));
            }
        },
        [datasetId, state.isLoading, onUpload]
    );
    const ret = useMemo(() => ({ ...state, uploadFile }), [state, uploadFile]);

    return ret;
};
