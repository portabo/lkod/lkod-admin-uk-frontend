import { useCallback, useEffect, useMemo, useReducer } from "react";

import { transformToFilesArray } from "@/utils/helpers";

import { dataServiceClient } from "../(services)/client/datasets-service-client";

interface State {
    isLoading: boolean;
    hasFailed: boolean;
    files: string[];
}

type Action = { type: "INITIAL" | "REQUEST_DATA" | "ERROR" } | { type: "DATA_RECEIVED"; payload: string[] };

const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case "REQUEST_DATA":
            return { isLoading: true, hasFailed: false, files: [] };
        case "ERROR":
            return { isLoading: false, hasFailed: true, files: [] };
        case "DATA_RECEIVED":
            return {
                isLoading: false,
                hasFailed: false,
                files: action.payload,
            };
        case "INITIAL":
            return { ...state };
    }
};
export const useFilesState = (datasetId: string | null) => {
    const [state, dispatch] = useReducer(reducer, {
        isLoading: false,
        hasFailed: false,
        files: [],
    });

    const refreshFiles = useCallback(async () => {
        if (datasetId) {
            dispatch({ type: "REQUEST_DATA" });
            try {
                const filesResponse = await dataServiceClient.getAllFiles({ datasetId });
                const filesData = transformToFilesArray(filesResponse);
                dispatch({ type: "DATA_RECEIVED", payload: filesData });
            } catch (error) {
                dispatch({ type: "ERROR" });
            }
        }
    }, [datasetId]);

    useEffect(() => {
        refreshFiles();
    }, [refreshFiles]);

    const ret = useMemo(() => ({ ...state, refreshFiles }), [state, refreshFiles]);

    return ret;
};
