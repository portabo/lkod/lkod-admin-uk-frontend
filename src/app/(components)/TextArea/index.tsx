import React, { DetailedHTMLProps, FC, InputHTMLAttributes } from "react";

import styles from "./TextArea.module.scss";

interface ITextArea extends DetailedHTMLProps<InputHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement> {
    label: string;
    hideLabel?: boolean;
}
const TextArea: FC<ITextArea> = ({
    children,
    placeholder,
    label,
    hideLabel,
    value,
    onChange,
    onClick,
    autoComplete,
    ...restProps
}) => {
    return (
        <>
            <label className={styles["label"]}>
                {hideLabel ?? label}
                <span className={styles["input-wrapper"]}>
                    <textarea
                        className={styles["input-area-textarea"]}
                        placeholder={placeholder}
                        value={value}
                        onChange={onChange}
                        onClick={onClick}
                        autoComplete={autoComplete}
                        aria-label={label}
                        {...restProps}
                    />
                    <span className={styles["input-icon"]}>{children}</span>
                </span>
            </label>
        </>
    );
};

export default TextArea;
